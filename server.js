const express = require('express');
const path = require('path');
require('dotenv').config();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();
const expressSession = require('express-session');
const SessionStore = require('express-session-sequelize')(expressSession.Store);
const db = require('./models/index.js');
const expressWs = require('express-ws')(app);
const port = process.env.PORT || 5000;
const session = require('express-session');
const withAuth = require('./middleware').withAuth;
app.use(bodyParser.json());

//production enviroment
if(process.env.NODE_ENV === 'production'){
    app.use(express.static(path.join(__dirname, 'client/build')));

    //redirect all non backend GET request to the index.html
    app.get(/^((?!\/api\/).)*$/, (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client/build/index.html'))
    })
}

//setup DB session storage
const sequelizeSessionStore = new SessionStore({
    db: db.sequelize,
});

app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(expressSession({
    secret: process.env.SESSION_SECRET,
    store: sequelizeSessionStore,
    maxAge: 30 * 86400 * 1000,
    resave: false,
    saveUninitialized: false
}));
app.use(require('flash')());

//import controllers
const UserController = require('./controllers/UserController');
const SearchController = require('./controllers/SearchController');
const RestaurantController = require('./controllers/RestaurantController');
const TalkController = require('./controllers/TalkController');
const FileController = require('./controllers/FileController');

//all API routes
app.post('/api/profile/sign-up',
    UserController.postRegistration.middleware,
    UserController.postRegistration.controller
);

app.delete('/api/delete-user/:id', UserController.deleteUser.controller);

app.post('/api/profile/sign-in',
    UserController.postLogin.controller
);

app.post('/api/restaurant/add-restaurant',
    RestaurantController.postAddRestaurant.middleware,
    RestaurantController.postAddRestaurant.controller
);

app.delete('/api/restaurant/delete-restaurant/:id', RestaurantController.deleteRestaurant.controller);
app.delete('/api/restaurant/delete-product/:id', RestaurantController.deleteProduct.controller);
app.delete('/api/restaurant/delete-contact/:id', RestaurantController.deleteContact.controller);

app.post('/api/restaurant/settings', RestaurantController.restaurantSettings.middleware, RestaurantController.restaurantSettings.controller);

app.post('/api/change-menu',
    RestaurantController.postChangeMenu.controller
);

app.post('/api/uploadImage', FileController.uploadImage.middleware, FileController.uploadImage.controller);

app.post('/api/uploadDocument', FileController.uploadDocument.middleware, FileController.uploadDocument.controller);

app.get('/api/checkToken', withAuth, function(req, res) {
    res.status(200).json({user: req.user});
});

app.get('/api/get-cities', SearchController.getCities.controller);

app.get('/api/get-restaurants', withAuth, RestaurantController.getRestaurantForUser.controller);
app.get('/api/get-ingredients', RestaurantController.getIngredients.controller);

app.get('/api/get-own-ingredients', withAuth, RestaurantController.getAvailableIngredients.controller);

app.get('/api/get-user-data', withAuth, UserController.getUserData.controller);

app.post('/api/profile/settings', UserController.settingsChange.controller);

app.post('/api/profile/logout', UserController.postLogout.controller);

app.ws('/api/receive-messages', TalkController.wsReceiveMessages.controller);

app.get('/api/get-history', withAuth, TalkController.getHistory.controller);

app.get('/api/find-restaurant/:city/:food*?', RestaurantController.findRestaurant.controller);

app.get('/api/restaurant/:id', RestaurantController.getRestaurant.controller);

app.post('/api/find-message', SearchController.findMessagesByText.controller);

app.listen(port, () => console.log(`Listening on port ${port}`));

module.exports = app;

