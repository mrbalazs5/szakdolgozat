'use strict';

module.exports = (sequelize, DataTypes) => {
    const ProductTag = sequelize.define('ProductTag', {
        productId: DataTypes.INTEGER,
        tagId: DataTypes.INTEGER
    }, {});

    ProductTag.associate = function(models) {
        ProductTag.belongsTo(models.Product, {foreignKey:{name: 'productId', allowNull: false}, as: 'product', onDelete: 'cascade', hooks: true});
        ProductTag.belongsTo(models.Tag, {foreignKey: {name: 'tagId', allowNull: false}, as: 'tag', onDelete: 'cascade', hooks: true});
    };

    return ProductTag;

};