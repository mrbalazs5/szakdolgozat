'use strict';

module.exports = (sequelize, DataTypes) => {
    const Restaurant = sequelize.define('Restaurant', {
        name: DataTypes.STRING,
        logo: DataTypes.STRING,
        website: DataTypes.STRING,
        userId: DataTypes.INTEGER,
        addressId: DataTypes.INTEGER,
        descriptionFile: DataTypes.STRING,
        menuFile: DataTypes.STRING,
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        }
    }, {});

    Restaurant.associate = function(models) {
        Restaurant.belongsTo(models.User, {foreignKey:'userId', as: 'user', onDelete : 'cascade'});
        Restaurant.belongsTo(models.Address, {foreignKey:'addressId', as: 'address', onDelete : 'cascade'});
        Restaurant.hasMany(models.Contact, {foreignKey:'restaurantId', as: 'contacts', onDelete : 'cascade'});
        Restaurant.hasOne(models.Menu, {foreignKey:'restaurantId', as: 'menu', onDelete : 'cascade'});
    };

    return Restaurant;

};