'use strict';

module.exports = (sequelize, DataTypes) => {
    const Address = sequelize.define('Address', {
        latitude: DataTypes.FLOAT,
        longitude: DataTypes.FLOAT,
        district: DataTypes.STRING,
        street: DataTypes.STRING,
        countryId: DataTypes.INTEGER,
        cityId: DataTypes.INTEGER,
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        }
    }, {});

    Address.associate = function(models) {
        Address.belongsTo(models.Country, {foreignKey:'countryId', as: 'country', onDelete : 'cascade'});
        Address.belongsTo(models.City, {foreignKey:'cityId', as: 'city', onDelete : 'cascade'});
    };

    return Address;

};