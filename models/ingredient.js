'use strict';

const types = {
    'spice': 0,
    'vegetable': 1,
    'fruit': 2,
    'meat': 2,
    'other': 3
};

module.exports = (sequelize, DataTypes) => {
    const Ingredient = sequelize.define('Ingredient', {
        name: DataTypes.STRING,
        type: DataTypes.INTEGER,
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        }
    }, {});

    Ingredient.getTypes = function(){
        return types;
    };

    Ingredient.associate = function(models) {
        Ingredient.belongsToMany(models.Product, {through: 'ProductIngredient', foreignKey: 'ingredientId', as: 'products', onDelete : 'cascade', hooks: true});
        Ingredient.belongsToMany(models.Tag, {through: 'IngredientTag', foreignKey: 'ingredientId', as: 'tags', onDelete : 'cascade', hooks: true});
    };

    return Ingredient;

};