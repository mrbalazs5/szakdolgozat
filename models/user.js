const bcrypt = require('bcrypt');

const saltRounds = 10;

const roles = {
    'user': 0,
    'admin': 1
};

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        email:   DataTypes.STRING,
        gender: DataTypes.STRING,
        age: DataTypes.INTEGER,
        avatar: DataTypes.STRING,
        password: DataTypes.STRING,
        role: DataTypes.INTEGER,
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        }
    }, {});

    User.associate = function(models) {
        User.hasMany(models.Restaurant, {foreignKey:'userId', as: 'restaurants', onDelete : 'cascade'});
        User.hasOne(models.History, {foreignKey:'userId', as: 'history', onDelete : 'cascade'});
    };

    User.beforeCreate((user, options) => {

        return bcrypt.hash(user.password, saltRounds)
            .then(hash => {
                user.password = hash;
            })
            .catch(err => {
                throw new Error();
            });
    });

    User.getRoles = function(){
        return roles;
    };

    User.prototype.validatePassword = function(password, callback){
        bcrypt.compare(password, this.password, function (err, isValid){

            if (err) {
                callback(err);
            } else {
                callback(err, isValid);
            }

        });
    };

    User.prototype.asyncValidatePassword = async function (password){
        return new Promise((resolve, reject) => {
            if(bcrypt.compareSync(password, this.password)){
                resolve(true);
            }else{
                reject(false);
            }
        });
    };

    return User;

};