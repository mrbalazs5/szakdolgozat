'use strict';

const types = {
    'client': 0,
    'bot': 1,
};

module.exports = (sequelize, DataTypes) => {
    const Message = sequelize.define('Message', {
        text: DataTypes.TEXT,
        type: DataTypes.STRING
    },{
        indexes: [
            { type: 'FULLTEXT', name: 'text_idx', fields: ['text'] }
        ]
    });

    Message.getTypes = function(){
        return types;
    };

    return Message;

};