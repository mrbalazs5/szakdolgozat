'use strict';

const types = {
    'food': 0,
    'drink': 1,
    'other': 2
};

module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('Product', {
        name: DataTypes.STRING,
        type: DataTypes.INTEGER,
        price: DataTypes.FLOAT,
        image: DataTypes.STRING
    }, {});

    Product.getTypes = function(){
        return types;
    };

    Product.associate = function(models) {
        Product.belongsTo(models.Menu, {foreignKey:'menuId', as: 'menu', onDelete: 'cascade'});
        Product.belongsToMany(models.Ingredient, {through: 'ProductIngredient', hooks: true, foreignKey: 'productId', as: 'ingredients', onDelete: 'cascade'});
        Product.belongsToMany(models.Tag, {through: 'ProductTag', foreignKey: 'productId', as: 'tags', onDelete: 'cascade', hooks: true});
    };

    return Product;

};