'use strict';

module.exports = (sequelize, DataTypes) => {
    const History = sequelize.define('History', {
        userId: DataTypes.INTEGER,
    }, {});

    History.associate = function(models) {
        History.hasMany(models.Message, {as: 'messages', onDelete : 'cascade'});
        History.belongsTo(models.User, {foreignKey:'userId', as: 'user', onDelete : 'cascade'});
    };

    return History;

};