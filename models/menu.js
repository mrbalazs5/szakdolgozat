'use strict';

module.exports = (sequelize, DataTypes) => {
    const Menu = sequelize.define('Menu', {
    }, {});

    Menu.associate = function(models) {
        Menu.belongsTo(models.Restaurant, {foreignKey:'restaurantId', as: 'restaurant', onDelete : 'cascade'});
        Menu.hasMany(models.Product, {foreignKey:'menuId', as: 'products', onDelete : 'cascade'});
    };

    return Menu;

};