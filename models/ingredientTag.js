'use strict';

module.exports = (sequelize, DataTypes) => {
    const IngredientTag = sequelize.define('IngredientTag', {
        ingredientId: DataTypes.INTEGER,
        tagId: DataTypes.INTEGER
    }, {});

    IngredientTag.associate = function(models) {
        IngredientTag.belongsTo(models.Ingredient, {foreignKey: {name: 'ingredientId', allowNull: false}, as: 'ingredient', onDelete : 'cascade'});
        IngredientTag.belongsTo(models.Tag, {foreignKey: {name: 'tagId', allowNull: false}, as: 'tag', onDelete : 'cascade'});
    };

    return IngredientTag;

};