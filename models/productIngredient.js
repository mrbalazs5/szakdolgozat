'use strict';

module.exports = (sequelize, DataTypes) => {
    const ProductIngredient = sequelize.define('ProductIngredient', {
        productId: DataTypes.INTEGER,
        ingredientId: DataTypes.INTEGER
    }, {});

    ProductIngredient.associate = function(models) {
        ProductIngredient.belongsTo(models.Product, {foreignKey: {name: 'productId', allowNull: false}, as: 'product', onDelete : 'cascade'});
        ProductIngredient.belongsTo(models.Ingredient, {foreignKey: {name: 'ingredientId', allowNull: false}, as: 'ingredient', onDelete : 'cascade'});
    };

    return ProductIngredient;

};