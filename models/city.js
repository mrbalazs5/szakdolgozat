'use strict';

module.exports = (sequelize, DataTypes) => {
    const City = sequelize.define('City', {
        name: DataTypes.STRING,
        postalCode: DataTypes.INTEGER,
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        },
        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: new Date()
        }
    }, {});

    City.associate = function(models) {

    };

    return City;

};