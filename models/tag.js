'use strict';

module.exports = (sequelize, DataTypes) => {
    const Tag = sequelize.define('Tag', {
        name: DataTypes.STRING
    }, {});

    Tag.associate = function(models) {
        Tag.belongsToMany(models.Product, {through: 'ProductTag', foreignKey: {name: 'tagId', allowNull: false}, as: 'products', onDelete : 'cascade', hooks: true});
        Tag.belongsToMany(models.Ingredient, {through: 'IngredientTag', foreignKey: {name: 'tagId', allowNull: false}, as: 'ingredients', onDelete : 'cascade', hooks: true});
    };

    return Tag;

};