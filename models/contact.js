'use strict';

module.exports = (sequelize, DataTypes) => {
    const Contact = sequelize.define('Contact', {
        name: DataTypes.STRING,
        phoneNumber: DataTypes.STRING,
        email: DataTypes.STRING
    }, {});

    Contact.associate = function(models) {
        Contact.belongsTo(models.Restaurant, {foreignKey:'restaurantId', as: 'restaurant', onDelete : 'cascade'});
    };

    return Contact;

};