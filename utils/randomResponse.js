//generates a random response based on the given messages
const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const randomResponse = (messages) => {
    return messages[getRandomInt(0, messages.length - 1)];
};

export default randomResponse;