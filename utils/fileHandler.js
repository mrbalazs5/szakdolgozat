import fs from 'fs';

//deletes the given file
const deleteFile = (filePath, cb) => {

    try{

         fs.stat(filePath, function (err, stats) {

            if (err) throw err;


             fs.unlink(filePath, function (err) {

                if(err) throw err;

                console.log('file deleted successfully:' + filePath);

                cb();

            });

        });

         return true;
    }catch (e) {
        console.log(e);
        return false;
    }

};

//creates a file
const createFile = (filePath, content) => {

    try{
        fs.writeFile(filePath, content, function (err) {
            if (err) throw err;
            console.log('File is created successfully: ' + filePath);
        });

        return true;
    }catch (e) {
        console.log(err);
        return false;
    }

};

const fileHandler = {
    delete: deleteFile,
    create: createFile
};

export default fileHandler;