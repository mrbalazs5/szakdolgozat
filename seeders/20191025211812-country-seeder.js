'use strict';

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Countries';
}else{
  tableName = 'countries';
}


module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert(tableName, [{
        name: 'Hungary',
        iso: 'HU'
      }], {});

  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.bulkDelete(tableName, null, {});

  }
};
