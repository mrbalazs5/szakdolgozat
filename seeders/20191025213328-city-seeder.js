'use strict';

const xlsx = require('xlsx');
const workbook = xlsx.readFile('./app/data/cities-hun.xls');
const sheet_name_list = workbook.SheetNames;

const cities = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Cities';
}else{
  tableName = 'cities';
}

module.exports = {
    up: (queryInterface, Sequelize) => {

        return queryInterface.bulkInsert(tableName, cities, {});

    },

    down: (queryInterface, Sequelize) => {

        return queryInterface.bulkDelete(tableName, null, {});

    }
};
