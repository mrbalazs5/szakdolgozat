'use strict';

const bcrypt = require('bcrypt');
const saltRounds = 10;

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Users';
}else{
  tableName = 'users';
}

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert(tableName, [{
          firstName: 'Admin',
          lastName: 'Admin',
          email: 'admin@t.com',
          gender: 'male',
          age: 23,
          avatar: '',
          password: bcrypt.hashSync('admin', saltRounds),
          role: 1
      }], {});

  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.bulkDelete(tableName, null, {});

  }
};
