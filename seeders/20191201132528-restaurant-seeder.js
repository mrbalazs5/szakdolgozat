'use strict';

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Restaurants';
}else{
  tableName = 'restaurants';
}

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert(tableName, [{
          name: 'Admin Restaurant',
          website: 'https://test.com',
          userId: 1,
          addressId: 1
      },
      {
          name: 'Admin Restaurant 2',
          website: 'https://test2.com',
          userId: 1,
          addressId: 1
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete(tableName, null, {});
  }
};
