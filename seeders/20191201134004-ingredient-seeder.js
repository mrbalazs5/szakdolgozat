'use strict';

const xlsx = require('xlsx');
const workbook = xlsx.readFile('./app/data/ingredients.xls');
const sheet_name_list = workbook.SheetNames;

const ingredients = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Ingredients';
}else{
  tableName = 'ingredients';
}

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert(tableName, ingredients, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete(tableName, null, {});
  }
};
