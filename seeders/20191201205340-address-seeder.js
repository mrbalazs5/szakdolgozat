'use strict';

let tableName = '';

if(process.platform === 'linux'){
  tableName = 'Addresses';
}else{
  tableName = 'addresses';
}

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert(tableName, [{
          latitude: 0.1,
          longitude: 0.1,
          district: 'Admin District',
          street: 'Admin Street',
          countryId: 1,
          cityId: 1
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete(tableName, null, {});
  }
};
