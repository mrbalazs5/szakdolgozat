const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
import {acceptedDocumentMimeTypes, acceptedImageMimeTypes} from '../utils/acceptedMimeTypes';

const uploadsPath = process.env.NODE_ENV === 'production' ? 'client/build/uploads' : 'client/public/uploads';

//this controller handles file uploads
const imageStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadsPath)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

const imageUpload = multer({ storage: imageStorage });

const documentStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadsPath + '/documents')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

const documentUpload = multer({ storage: documentStorage });

//removes unnecessary parts of the file path
const normalizeFilePath = (path) => {
    return '\\' + path
        .replace('client\\public\\','')
        .replace('\\client\/public\/','')
        .replace('client\/public\/','')
        .replace('client\\build\\','')
        .replace('\\client\/build\/','')
        .replace('client\/build\/','');
};

const FileController = {

    //handle image uploads
    uploadImage: {
        controller: (req, res, next) => {
            const file = req.file;

            console.log(file);

            if (!file){
                res.status(422).json({errors: ['Image can not be empty']});
            } else if(file && !acceptedImageMimeTypes.includes(file.mimetype)){
                res.status(422).json({errors: ['Image must be one of png, jpeg or svg']});
            }

            res.status(200).json({filePath: normalizeFilePath(req.file.path)});

        },
        middleware: [bodyParser.urlencoded({ extended: true }), imageUpload.single('image')]
    },
    //handle document uploads
    uploadDocument: {
        controller: (req, res, next) => {

            const file = req.file;

            console.log(file);

            if (!file){
                res.status(422).json({errors: ['The file field can not be empty']});
            } else if(file && !acceptedDocumentMimeTypes.includes(file.mimetype)){
                res.status(422).json({errors: ['The file must be one of docx or pdf types']});
            }

            res.status(200).json({filePath: '\\' + req.file.path.replace('client\\public\\',''), fileName: req.file.originalname});

        },
        middleware: [bodyParser.urlencoded({ extended: true }), documentUpload.single('document')]
    }

};

module.exports = FileController;