//DB init
const db = require('../models/index.js');
const jwt = require('jsonwebtoken');
const models = require('../models');
const bcrypt = require('bcrypt');
const saltRounds = 10;
import fileHandler from '../utils/fileHandler';


//Validator init
const { check, validationResult } = require('express-validator');
const User = models.User;
const uploadsDir = process.env.NODE_ENV === 'production' ? './client/build' : './client/public';

//handles all user requests
const UserController = {

    postRegistration: {
        controller : (req, res) => {

            const errors = validationResult(req);

            const {firstName, lastName, email, gender, avatar, age, password} = req.body;

            if(!errors.isEmpty()){

                let errorMessages = [];

                errors.errors.map(function (error) {
                    errorMessages.push(error.msg);
                });

                return res.status(422).json({errors: errorMessages});
            }

            User.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                gender: gender,
                age: age,
                avatar: avatar,
                password: password,
                role: User.getRoles()['user']
            })
            .then(user => {
                console.log("User's auto-generated ID:", user.id);
                return res.json({success: [
                        'Registration completed successfully',
                        'You can sign in now'
                    ]});
            })
            .catch(errors => {
                return res.status(422).json({errors: ['Database validation error']});
            });

        },
        middleware: [

            check('firstName')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('Firstname can not be empty'),
            check('lastName')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('Lastname can not be empty'),
            check('email')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Email can not be empty')
                .isEmail()
                .withMessage('Email must be a valid email address')
                .normalizeEmail()
                .custom(value => {
                    return User.findOne({ where: {email: value} }).then(user => {
                        if(user) {
                            return Promise.reject('E-mail already in use');
                        }
                    });
                }),
            check('gender')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Gender can not be empty')
                .isIn(['male', 'female', 'other'])
                .withMessage('Gender must be one of male, female, other'),
            check('age')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Age can not be empty')
                .isInt({ min: 3, max: 200 })
                .withMessage('Age must be an integer between 3 and 200'),
            check('avatar')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Avatar can not be empty'),
            check('password')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Password can not be empty')

        ]
    },
    postLogin: {
        controller: (req, res) => {

            const { email, password } = req.body;

            User.findOne({ where: {email: email} })
                .then(userResult => {

                    userResult.validatePassword(password, function (err, isValid) {

                        if (err) {
                            res.status(500)
                                .json({
                                    errors: ['Internal error please try again']
                                });
                        } else if (!isValid) {
                            res.status(401)
                                .json({
                                    errors: ['Incorrect password']
                                });
                        } else {
                            const role = userResult.role;
                            const avatar = userResult.avatar;
                            const payload = { email, avatar, role};
                            const token = jwt.sign(payload, process.env.JWT_SECRET);
                            res.cookie('token', token, { httpOnly: true, maxAge: 900000})
                                .status(200)
                                .json({
                                    success: ['You have successfully signed in']
                                });
                        }

                    });

                }).catch(err => {
                    console.log(err);
                    res.status(401)
                    .json({
                        errors: ['Incorrect email']
                    });
                });

        }
    },
    postLogout: {
        controller: (req, res) => {
            return res.clearCookie('token').status(200).json({info: ['You have successfully logged out']});
    }},
    getUserData: {
        controller: (req, res) => {
            const { email } = req.user;

            User.findOne({where: {email: email}, include: 'restaurants'})
                .then((user) => {
                    return res.status(200).send(user.get());
                })
                .catch((err) => {
                    console.log(err);
                    return res.status(404);
                });

        }
    },
    //settings page form handler
    settingsChange: {
        controller: (req, res) => {
            const errors = validationResult(req);

            if(!errors.isEmpty()){

                let errorMessages = [];

                errors.errors.map(function (error) {
                    errorMessages.push(error.msg);
                });

                return res.status(422).json({errors: errorMessages});
            }

            const {firstName, lastName, email, gender, avatar, age, password, newPassword, userId} = req.body;

            User.findByPk(userId)
                .then((user) => {

                    user.validatePassword(password, function (err, isValid) {

                        if (err) {
                            res.status(500)
                                .json({
                                    errors: ['Internal error please try again']
                                });
                        } else if (!isValid) {
                            res.status(401)
                                .json({
                                    errors: ['Incorrect password']
                                });
                        } else {

                            let hasChanged = false;

                            if(firstName && user.firstName !== firstName){
                                hasChanged = true;
                                user.update({
                                    firstName: firstName
                                });
                            }

                            if(lastName && user.lastName !== lastName){
                                hasChanged = true;
                                user.update({
                                    lastName: lastName
                                });
                            }

                            if(email && user.email !== email){
                                hasChanged = true;

                                const role = user.role;
                                const avatar = user.avatar;
                                const payload = { email, avatar, role};
                                const token = jwt.sign(payload, process.env.JWT_SECRET);

                                res.clearCookie('token').cookie('token', token, { httpOnly: true });

                                user.update({
                                    email: email
                                });
                            }

                            if(gender && user.gender !== gender){
                                hasChanged = true;
                                user.update({
                                    gender: gender
                                });
                            }

                            if(age && user.age !== age){
                                hasChanged = true;
                                user.update({
                                    age: age
                                });
                            }

                            if(newPassword){
                                hasChanged = true;
                                user.update({
                                    password: bcrypt.hashSync(newPassword, saltRounds)
                                });
                            }

                            if(avatar && user.avatar !== avatar){
                                hasChanged = true;

                                const role = user.role;
                                const email = user.email;
                                const payload = { email, avatar, role};
                                const token = jwt.sign(payload, process.env.JWT_SECRET);

                                res.clearCookie('token').cookie('token', token, { httpOnly: true });

                                user.update({
                                    avatar: avatar
                                });
                            }

                            if(hasChanged){
                                return res.json({success: [
                                        'Settings has been changed'
                                    ]});
                            }else{
                                return res.json({success: [
                                        'Nothing has changed'
                                    ]});
                            }

                        }

                    });

                })
                .catch(err => {
                    console.log(err);
                    res.status(401)
                        .json({
                            errors: ['You must be logged in']
                        });
                });

        },
        middleware: [

            check('firstName')
                .trim()
                .escape(),
            check('lastName')
                .trim()
                .escape(),
            check('email')
                .isEmail()
                .withMessage('Email must be a valid email address')
                .normalizeEmail()
                .custom(value => {
                    return User.findOne({ where: {email: value} }).then(user => {
                        if(user) {
                            return Promise.reject('E-mail already in use');
                        }
                    });
                }),
            check('gender')
                .isIn(['male', 'female', 'other'])
                .withMessage('Gender must be one of male, female, other'),
            check('age')
                .isInt({ min: 3, max: 200 })
                .withMessage('Age must be an integer between 3 and 200'),
            check('password')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Password can not be empty')

        ]
    },
    deleteUser: {
        controller: (req, res) => {
            const { id } = req.params;

            //delete the user's avatar
            User.findByPk(id)
                .then((user) =>{

                    const filePath = uploadsDir + user.avatar;

                    fileHandler.delete(filePath, function () {

                        //delete the user
                        User.destroy({
                            where: {id: id}
                        })
                            .then((deletedRowsNum) => {

                                if(deletedRowsNum >= 1){
                                    return res.clearCookie('token').status(200).json({success: [
                                            'Your account has been deleted'
                                        ]});
                                }else{
                                    res.status(401)
                                        .json({
                                            errors: ['Can not find user with the given id']
                                        });
                                }

                            })
                            .catch(err => {
                                console.log(err);
                                res.status(401)
                                    .json({
                                        errors: ['Can not find user with the given id']
                                    });
                            });

                    });

                })
                .catch(err => {
                    console.log(err);
                    return res.status(401)
                        .json({
                            errors: ['Can not find user with the given id']
                        });
                });

        }
    }

};

module.exports = UserController;
