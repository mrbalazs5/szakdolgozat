import TalkHandler from "../app/TalkHandler";
const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;
const models = require('../models');
const User = models.User;
const Message = models.Message;
const History = models.History;

//saves the given message into the DB
const saveMessage = (messages, token, messageType) => {

    const messageTypes = Message.getTypes();

      //if logged in
      jwt.verify(token, secret, function(err, user) {
        if (!err) {

             User.findOne({where: {email: user.email}, include: 'history'})
                .then((userEntity) => {

                    //if the user has a message history
                    if(userEntity.get().history){

                        messages.map((message) => {

                            let messageEntity = Message.build({
                                type: messageTypes[messageType],
                                text: message
                            });

                            messageEntity.save()
                                .then((savedMessage) => {
                                    userEntity.get().history.addMessage(savedMessage);
                                })

                        });




                    }else{

                        History.create()
                            .then((history) => {
                                history.setUser(userEntity);
                                userEntity.setHistory(history);

                                messages.map((message) => {

                                    let messageEntity = Message.build({
                                        type: messageTypes[messageType],
                                        text: message
                                    });

                                    messageEntity.save()
                                        .then((savedMessage) => {
                                            history.addMessage(savedMessage);
                                        })

                                });

                            })
                            .catch((err) => {
                                console.log(err);
                            });

                    }

                })
                .catch((err) => {
                    console.log(err);
                });

        }
    });

};

//generates a random integer
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//generates a random welcome message
const getRandomWelcome = () => {

    let randomWelcomes = [
      {text:['Hello, i\'m ChefBot!', 'Are you hungry?'], thought: 'want_to_eat_thought'},
      {text:['Hi. My name is ChefBot', 'Looking for a good restaurant?'], thought: 'want_to_eat_thought'},
      {text:['Hi. My name is ChefBot', 'Do you want to eat something?'], thought: 'want_to_eat_thought'},
    ];
    return randomWelcomes[getRandomInt(0, randomWelcomes.length - 1)];

};

//this controller handles all talk request
const TalkController = {
    wsReceiveMessages : {
        controller: (ws, req) => {

            let th = new TalkHandler();

            ws.on('message', function(msg) {
                let message = JSON.parse(msg);

                //user's auth token
                const token =
                    req.body.token ||
                    req.query.token ||
                    req.headers['x-access-token'] ||
                    req.cookies.token;

                //if the user visits the page for the first time
                if(message.message.firstContact){

                    const randomWelcome = getRandomWelcome();
                    const randomWelcomeTexts = randomWelcome.text;

                    if(token){

                        saveMessage(randomWelcomeTexts, token, 'bot');

                    }

                    th.think(randomWelcome.thought)
                        .then(() => {

                            randomWelcomeTexts.map((welcomeMessage) => {

                                ws.send(JSON.stringify({text: welcomeMessage, wait: false, date: new Date()}));

                            });

                        });

                    return;

                }


                if(token){

                    let messageText = message.message;

                    //if the message is a file then the message that will be saved becomes a HTML code
                    if(message.message.image){
                        messageText =  `<img src="${message.message.image}"/>`;
                    }else if(message.message.document){
                        const path = message.message.document.path;
                        const name = message.message.document.name;
                        messageText = `<a target="_blank" class="message-document" href="${path}">${name}<a>`;
                    }

                    if(message.hideInput){
                        messageText = 'HIDDEN MESSAGE';
                    }

                    saveMessage([messageText], token, 'client');
                }

                let listenMessage = message.message;

                //if the message is a file then the message that will be sent to Dialogflow becomes the file's path
                if(message.message.image){
                    listenMessage = message.message.image;
                }else if(message.message.document){
                    listenMessage = message.message.document.path;
                }

                //the bot's answer to the client's message
                th.listen(listenMessage)
                    .then((response) => {

                        if(typeof response === 'object' && response.constructor === Object){

                            if(token){
                                saveMessage([response.text], token, 'bot');
                            }

                            ws.send(JSON.stringify({
                                ...response,
                                wait: true,
                                date: new Date()
                            }));

                        }else{

                            if(token){
                                saveMessage([response], token, 'bot');
                            }

                            ws.send(JSON.stringify({text: response, wait: true, date: new Date()}));

                        }

                    })
                    .catch((e) => {
                        console.log(e);
                        return false;
                    });



            });

        }
    },
    //get message history
    getHistory: {
        controller: (req, res) => {

            const { email } = req.user;

            User.findOne({ where:{email: email}, include: {association: 'history', include: 'messages'} })
                .then((user) => {
                    if(user && user.get().history){
                        return res.status(200).send(user.get().history);
                    }else{
                        return res.status(404).send();
                    }
                })
                .catch((err) => {
                    console.log(err);
                    return res.status(404).send();
                });

        }
    }
};

module.exports = TalkController;