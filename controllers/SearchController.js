const db = require('../models/index.js');
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;

const models = require('../models');
const City = models.City;
const Message = models.Message;

//this controller handles all search request
const SearchController = {
    getCities: {
        controller: (req, res) => {
            const { expression } = req.query;

            City.findAll({
                attributes: [sequelize.fn('DISTINCT', sequelize.col('name')), 'name'],
                where: {
                    name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', `%${expression}%`)
                },
                limit: 10
            }).then((cities) => {
                res.send(cities);
            });

        }
    },
    findMessagesByText: {
        controller: (req, res) => {

            const { searchText } = req.body;

            Message.findAll({
                where: Sequelize.literal('MATCH (text) AGAINST (:expression)'),
                replacements: {
                    expression: searchText
                }
            }).then((result) => {

                res.json(result);
            });

        }
    }
};

module.exports = SearchController;