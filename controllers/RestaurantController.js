import {restaurantTemplate} from "../app/MessageTemplates";
import fileHandler from '../utils/fileHandler';
const db = require('../models/index.js');
const sequelize = db.sequelize;
const models = require('../models');
import DialogFlow from '../app/Dialogflow';
const Op = db.Sequelize.Op;

const Restaurant = models.Restaurant;
const Country = models.Country;
const City = models.City;
const User = models.User;
const Address = models.Address;
const Contact = models.Contact;
const Ingredient = models.Ingredient;
const Menu = models.Menu;
const Product = models.Product;
const Tag = models.Tag;
import { restaurantQuery } from '../app/Queries';

const { check, validationResult } = require('express-validator');

const uploadsDir = process.env.NODE_ENV === 'production' ? './client/build' : './client/public';

//this controller handles all restaurant request
const RestaurantController = {
    postAddRestaurant: {
        controller: (req, res) => {

            const errors = validationResult(req);

            const {
                name,
                logo,
                website,
                country,
                city,
                district,
                street,
                latitude,
                longitude,
                contacts,
                userIn,
                descriptionFile,
                menuFile,
            } = req.body;

            if(!errors.isEmpty()){

                let errorMessages = [];

                errors.errors.map(function (error) {
                    errorMessages.push(error.msg);
                });

                return res.status(422).json({errors: errorMessages});
            }

            Address.create({
                    latitude: latitude,
                    longitude: longitude,
                    district: district,
                    street: street,
                    countryId: country.get().id,
                    cityId: city.get().id,
            }).then(addressEntity => {

                Restaurant.create({
                        name: name,
                        logo: logo,
                        website: encodeURI(website),
                        userId: userIn.get().id,
                        addressId: addressEntity.get().id,
                        descriptionFile: descriptionFile,
                        menuFile: menuFile
                }).then(restaurantEntity => {

                    userIn.update({restaurantId: restaurantEntity.id});

                    contacts.map((contact) => {

                        Contact.create({
                            name: contact.name,
                            phoneNumber: contact.phoneNumber,
                            email: contact.email,
                            restaurantId: restaurantEntity.id
                        }).catch(errors => {
                            console.log(errors);
                        });

                    });

                    console.log("Restaurant's auto-generated ID:", restaurantEntity.id);

                    return res.json({success: [
                            'Restaurant created successfully'
                    ]});

                }).catch(errors => {
                    console.log(errors);
                    return res.status(422).json({errors: ['Database validation error']});
                });

            }).catch(errors => {
                console.log(errors);
                return res.status(422).json({errors: ['Database validation error']});
            });

        },
        middleware: [

            check('name')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('Name can not be empty'),
            check('logo')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Logo can not be empty'),
            check('website')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Website can not be empty'),
            check('country')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('Country can not be empty')
                .custom((value, { req }) => {
                    return Country
                        .findOne({
                            where: {name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', `%${value.toLowerCase()}%`)}
                        })
                        .then(country => {
                            if(country) {
                                req.body.country = country;
                            }else{
                                return Promise.reject('Can not find this country in the database');
                            }
                        });
                }),
            check('city')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('City can not be empty')
                .custom((value, { req }) => {
                    return City
                        .findOne({
                            where: {name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', `%${value.toLowerCase()}%`)}
                        })
                        .then(city => {
                            if(city) {
                                req.body.city = city;
                            }else{
                                return Promise.reject('Can not find this city in the database');
                            }
                        });
                }),
            check('district')
                .trim()
                .escape(),
            check('street')
                .exists({checkNull: true, checkFalsy: true})
                .trim()
                .escape()
                .withMessage('Street can not be empty'),
            check('latitude')
                .isFloat()
                .withMessage('Latitude must be a floating point number'),
            check('longitude')
                .isFloat()
                .withMessage('Longitude must be a floating point number'),
            check('contacts')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('Contacts can not be empty')
                .isArray()
                .withMessage('Contacts must be an array'),
            check('user')
                .exists({checkNull: true, checkFalsy: true})
                .withMessage('User can not be empty')
                .custom((value, { req }) => {
                    return User.findOne({ where: {email: value} }).then(user => {
                        if(user) {
                            req.body.userIn = user;
                        }else{
                            return Promise.reject('Can not find user in the database');
                        }
                    });
                })

        ]
    },
    postChangeMenu: {
        controller: (req, res) => {
            const { products } = req.body;

            let errorMessages = [];
            let successMessages = [];
            const df = new DialogFlow(process.env.DIALOGFLOW_PROJECT_ID, 'en-US');

            let foods = [];
            let drinks = [];
            let others = [];

            let productPromises = products.map((product) => {
                switch(parseInt(product.productType)){
                    case 0:
                        foods.push({value: product.productName, synonyms: [product.productName]});
                        break;
                    case 1:
                        drinks.push({value: product.productName, synonyms: [product.productName]});
                        break;
                    case 2:
                        others.push({value: product.productName, synonyms: [product.productName]});
                        break;

                }

                return Restaurant.findOne({where: {id: product.restaurant.id}, include: 'menu'})
                    .then((restaurant) => {

                         return Product.create({
                            name: product.productName,
                            type: product.productType,
                            price: product.productPrice,
                            image: product.productImage,
                        }).then((productEntity) => {

                            for(let ingredientName in product.ingredients){

                                 Ingredient.findOne({where: {name: ingredientName}})
                                .then((ingredientEntity) => {

                                    if(product.ingredients[ingredientName]){

                                        productEntity.addIngredient(ingredientEntity);

                                    }

                                })
                                .catch(errors => {
                                    console.log(errors);
                                    errorMessages.push(`Can not find ingredient with name: ${ingredientName}`);
                                });

                            }

                            product.tags.map((tag) => {
                                 Tag.create({
                                    name: tag.tagName
                                }).then((tagEntity) => {
                                    productEntity.addTag(tagEntity);
                                     df.addEntities('tag', [{value: tagEntity.name, synonyms: [tagEntity.name]}]);
                                })
                            });

                            if(restaurant.get().menu){
                                let menu = restaurant.get().menu;

                                menu.addProduct(productEntity);

                                successMessages.push(`${restaurant.get().name}'s menu has been changed successfully`);

                            }else{
                                return Menu.create()
                                    .then((menu) => {
                                        menu.setRestaurant(restaurant);
                                        menu.addProduct(productEntity);
                                        productEntity.setMenu(menu);

                                        successMessages.push(`${restaurant.get().name}'s menu has been created successfully`);

                                    });
                            }

                        });



                    })
                    .catch(errors => {
                        console.log(errors);
                        errorMessages.push(`Can not find restaurant with id: ${product.restaurant}`);
                    });

            });


            Promise.all(productPromises)
                .then(() => {

                    if(foods.length > 0){
                        df.addEntities('food', foods);
                    }

                    if(drinks.length > 0){
                        df.addEntities('drink', drinks);
                    }

                    if(others.length > 0){
                        df.addEntities('other_product', others);
                    }

                    return res.status(200).json({success: successMessages, errors: errorMessages});
                });


        }
    },
    getRestaurantForUser : {
        controller: (req, res) => {

            const { email } = req.user;

            User.findOne({ where: {email: email}, include: [{
                    association: 'restaurants',
                    include: ['user', {association: 'address', include: ['country', 'city']}, 'contacts', {association: 'menu', include: 'products'}]
                }] })
                .then((user) => {

                    if(user){
                        return res.status(200).send(user.get().restaurants);
                    }else{
                        return res.status(404);
                    }

                }).catch((err) => {
                    console.log(err);
                    return res.status(404);
                });

        }
    },
    getIngredients : {
        controller: (req, res) => {

            Ingredient.findAll()
                .then((ingredients) => {
                    res.status(200).send(ingredients);
                }).catch((err) => {
                    console.log(err);
                    return res.status(404);
                });

        }
    },
    getAvailableIngredients : {
        controller: (req, res) => {

            const { email } = req.user;

            User.findOne({ where: {email: email}, include: [{
                    association: 'restaurants',
                    include: [
                        'user',
                        'contacts',
                        {association: 'address', include: ['country', 'city']},
                        {association: 'menu', include: 'ingredients'}
                    ]
                }] })
                .then((user) => {

                    const restaurants  = user.get().restaurants;
                    let availableIngredients = [];

                    if(restaurants){
                        restaurants.map((restaurant) => {

                            const menu = restaurant.get().menu;

                            if(menu){

                                menu.get().ingredients.map((ingredient) => {
                                    availableIngredients.push(ingredient);
                                });

                            }

                        });
                    }

                    if(availableIngredients.length > 0){
                        res.status(200).send(availableIngredients);
                    }else{
                        return res.status(404);
                    }


                }).catch((err) => {
                console.log(err);
                return res.status(404);
            });

        }
    },
    findRestaurant: {
        controller: (req, res) => {

            const { city, food } = req.params;

            restaurantQuery(city, food)
                .then((filteredRestaurants) => {
                    res.status(200).send(filteredRestaurants);
                }).catch((err) => {
                    console.log(err);
                    return res.status(404);
                });

        }
    },
    getRestaurant: {
        controller: (req, res) => {

            const { id } = req.params;

            Restaurant.findByPk(id)
                .then((restaurant) => {
                    res.status(200).send(restaurant);
                }).catch((err) => {
                console.log(err);
                return res.status(404);
            });

        }
    },
    deleteRestaurant: {
        controller: (req, res) => {
            const { id } = req.params;

            //delete the restaurant's logo
            Restaurant.findByPk(id)
                .then((rest) =>{

                    const filePath = uploadsDir + rest.logo;

                    fileHandler.delete(filePath, function () {
                        //delete the restaurant
                        Restaurant.destroy({
                            where: {id: id}
                        })
                            .then((deletedRowsNum) => {

                                if(deletedRowsNum >= 1){
                                    return res.status(200).json({success: [
                                            'Restaurant has been deleted'
                                        ]});
                                }else{
                                    res.status(401)
                                        .json({
                                            errors: ['Can not find restaurant with the given id']
                                        });
                                }

                            })
                            .catch(err => {
                                console.log(err);
                                return res.status(401)
                                    .json({
                                        errors: ['Can not find restaurant with the given id']
                                    });
                            });
                    });

                })
                .catch(err => {
                    console.log(err);
                    return res.status(401)
                        .json({
                            errors: ['Can not delete the given file']
                        });
                });

        }
    },
    deleteProduct: {
        controller: (req, res) => {
            const { id } = req.params;

            Product.findByPk(id)
                .then((prod) =>{

                    const filePath = uploadsDir + prod.image;

                    fileHandler.delete(filePath, function () {

                        //delete the product
                        Product.destroy({
                            where: {id: id}
                        })
                            .then((deletedRowsNum) => {

                                if(deletedRowsNum >= 1){
                                    return res.status(200).json({success: [
                                            'Product has been deleted'
                                        ]});
                                }else{
                                    res.status(401)
                                        .json({
                                            errors: ['Can not find product with the given id']
                                        });
                                }

                            })
                            .catch(err => {
                                console.log(err);
                                res.status(401)
                                    .json({
                                        errors: ['Can not find product with the given id']
                                    });
                            });

                    });

                })
                .catch(err => {
                    console.log(err);
                    return res.status(401)
                        .json({
                            errors: ['Can not delete the given file']
                        });
                });

        }
    },
    deleteContact: {
        controller: (req, res) => {
            const { id } = req.params;

            Contact.destroy({
                where: {id: id}
            })
                .then((deletedRowsNum) => {

                    if(deletedRowsNum >= 1){
                        return res.status(200).json({success: [
                                'Contact has been deleted'
                            ]});
                    }else{
                        res.status(401)
                            .json({
                                errors: ['Can not find contact with the given id']
                            });
                    }

                })
                .catch(err => {
                    console.log(err);
                    res.status(401)
                        .json({
                            errors: ['Can not find contact with the given id']
                        });
                });

        }
    },
    restaurantSettings: {
        controller: (req, res) => {

            const errors = validationResult(req);

            if(!errors.isEmpty()){

                let errorMessages = [];

                errors.errors.map(function (error) {
                    errorMessages.push(error.msg);
                });

                return res.status(422).json({errors: errorMessages});
            }

            const {
                restaurant,
                name,
                logo,
                website,
                country,
                city,
                district,
                street,
                latitude,
                longitude,
                contacts,
                descriptionFile,
                menuFile,
            } = req.body;

            Restaurant.findByPk(restaurant.id, { include: [
                'user',
                {
                    association: 'address',
                    include: ['country', 'city']
                },
                'contacts',
                {
                    association: 'menu',
                    include: 'products'
                }
            ]
            })
                .then((restaurantEnt) => {

                    let hasChanged = false;

                    if(name && restaurantEnt.name !== name){
                        hasChanged = true;
                        restaurantEnt.update({
                            name: name
                        });
                    }

                    if(logo && restaurantEnt.logo !== logo){
                        hasChanged = true;

                        if(restaurantEnt.logo){

                            const filePath = uploadsDir + restaurantEnt.logo;

                            fileHandler.delete(filePath, function (){});

                        }

                        restaurantEnt.update({
                            logo: logo
                        });


                    }

                    if(website && restaurantEnt.website !== website){
                        hasChanged = true;
                        restaurantEnt.update({
                            website: website
                        });
                    }

                    if(country && restaurantEnt.address.country.id !== country.id){
                        hasChanged = true;
                        restaurantEnt.address.update({
                            countryId: country.id
                        });
                    }

                    if(city && restaurantEnt.address.city.id !== city.id){
                        hasChanged = true;
                        console.log('aaaaaaaa');
                        console.log(city);
                        restaurantEnt.address.update({
                            cityId: city.id
                        });
                    }

                    if(district && restaurantEnt.address.district !== district){
                        hasChanged = true;
                        restaurantEnt.address.update({
                            district: district
                        });
                    }

                    if(street && restaurantEnt.address.street !== street){
                        hasChanged = true;
                        restaurantEnt.address.update({
                            street: street
                        });
                    }

                    if(latitude && restaurantEnt.address.latitude !== latitude){
                        hasChanged = true;
                        restaurantEnt.address.update({
                            latitude: latitude
                        });
                    }

                    if(longitude && restaurantEnt.address.longitude !== longitude){
                        hasChanged = true;
                        restaurantEnt.address.update({
                            longitude: longitude
                        });
                    }

                    if(descriptionFile && restaurantEnt.descriptionFile !== descriptionFile){
                        hasChanged = true;

                        if(restaurantEnt.descriptionFile){

                            const filePath = uploadsDir + restaurantEnt.descriptionFile;

                            fileHandler.delete(filePath, function (){});

                        }


                        restaurantEnt.update({
                            descriptionFile: descriptionFile
                        });

                    }

                    if(menuFile && restaurantEnt.menuFile !== menuFile){
                        hasChanged = true;

                        if(restaurantEnt.menuFile){

                            const filePath = uploadsDir + restaurantEnt.menuFile;

                            fileHandler.delete(filePath, function (){});

                        }


                        restaurantEnt.update({
                            menuFile: menuFile
                        });

                    }

                    if(contacts){
                        hasChanged = true;

                        contacts.map((contact) => {

                            Contact.create({
                                name: contact.name,
                                phoneNumber: contact.phoneNumber,
                                email: contact.email,
                                restaurantId: restaurantEnt.id
                            }).catch(errors => {
                                console.log(errors);
                            });

                        });

                    }

                    if(hasChanged){
                        return res.json({success: [
                                'Settings has been changed'
                            ]});
                    }else{
                        return res.json({success: [
                                'Nothing has changed'
                            ]});
                    }

                })
                .catch(err => {
                    console.log(err);
                    res.status(401)
                        .json({
                            errors: ['Can not find restaurnt with id:' + restaurant.id]
                        });
                });

        },
        middleware: [

            check('name')
                .trim()
                .escape(),
            check('country')
                .trim()
                .escape()
                .custom((value, { req }) => {
                    return Country
                        .findOne({
                            where: {name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', `%${value.toLowerCase()}%`)}
                        })
                        .then(country => {
                            if(country) {
                                req.body.country = country;
                            }else{
                                return Promise.reject('Can not find this country in the database');
                            }
                        });
                }),
            check('city')
                .trim()
                .escape()
                .custom((value, { req }) => {

                    if(value){
                        return City
                            .findOne({
                                where: {name: sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), 'LIKE', `%${value.toLowerCase()}%`)}
                            })
                            .then(city => {
                                if(city) {
                                    req.body.city = city;
                                }else{
                                    return Promise.reject('Can not find this city in the database');
                                }
                            });
                    }else{
                        req.body.city = '';
                        return Promise.resolve(false);
                    }


                }),
            check('district')
                .trim()
                .escape(),
            check('street')
                .trim()
                .escape(),
            check('latitude')
                .isFloat()
                .withMessage('Latitude must be a floating point number'),
            check('longitude')
                .isFloat()
                .withMessage('Longitude must be a floating point number'),
            check('contacts')
                .isArray()
                .withMessage('Contacts must be an array')
        ]
    }
};

module.exports = RestaurantController;