import uuid from 'uuid';

//General NLP API class
class TalkAPI{

    constructor(apiObject, language){
        this.sessionId =  uuid.v4();
        this.apiObject = apiObject;
        this.language = language;
    }

    sendRequest(){
        return new Error('You have to implement the method sendRequest!');
    }

    getSingleResponse(){
        return new Error('You have to implement the method getSingleResponse!');
    }

    createContext(){
        return new Error('You have to implement the method createContext!');
    }

    deleteContext(){
        return new Error('You have to implement the method deleteContext!');
    }

    listContexts(){
        return new Error('You have to implement the method listContexts!');
    }

}

export default TalkAPI;