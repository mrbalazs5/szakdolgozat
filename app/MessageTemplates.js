//Message templates for the chatbot

export const restaurantTemplate = (restaurant) => {
    const logo = restaurant.get().logo ? `<img src="${restaurant.get().logo}">` : ``;
    return(
        `
        ${logo}
        <h2>${restaurant.get().name}</h2>
        <div>
            ${restaurant.get().address.get().city.get().postalCode}, 
            ${restaurant.get().address.get().city.get().name}, 
            ${restaurant.get().address.get().district} 
            ${restaurant.get().address.get().street}
        </div>
        <a target="_blank" href="${restaurant.get().website}">Link to their website</a>
        `
    );
};

export const productTemplate = (product) => {
    const image = product.get().image ? `<img src="${product.get().image}">` : ``;
    return(
        `
        <div class="product-list-item">
            ${image}
            <h2>${product.get().name}</h2>
            <div>
                ${product.get().price}€
            </div>
        </div>
        `
    );
};