const db = require('../models/index.js');
const Op = db.Sequelize.Op;
const models = require('../models');
import { restaurantTemplate, productTemplate } from './MessageTemplates';
import { restaurantQuery, productQuery } from './Queries';
import validator from 'validator';
import randomResponse from '../utils/randomResponse';
import { struct } from 'pb-util';

const User = models.User;
const Restaurant = models.Restaurant;

//generates responses with specific template, limit, texts
const responseGenerator = (dataArray, limit = 5, emptyResponse, foundText = 'I found these for you', moreText = 'More', moreLink, template) => {
    let responseText = '';
    let i;

    for(i = 0; i < dataArray.length; i++){

        if(i > limit){
            break;
        }

        responseText += template(dataArray[i]);

    }

    if(!responseText){
        responseText = emptyResponse;
    }else{

        if(i >= limit){
            responseText += `<a class="more" target="_blank" href="${moreLink}">${moreText}</a>`
        }

        responseText = `<div class="message-text">${foundText}: </div>` + responseText;
    }

    return responseText;
};

const getContextIdFromContextName = (name) => {
    return name.substring(name.lastIndexOf("/") + 1, name.length);
};

//resets the authentication flow
const resetAuthentication = (handler, contexts) => {

    //password intent
    handler.deleteContext(getContextIdFromContextName(contexts[0].name));
    //email intent
    handler.deleteContext(getContextIdFromContextName(contexts[1].name));
    //refresh the authenticable intent
    handler.createContext(getContextIdFromContextName(contexts[2].name),{}, 4);

    handler.listContexts();

};

//these functions generates a response bsed on the intent parameters and the database query results
const queryHandlers = {
    hungryWhere: (parameters) => {

        const city = parameters.fields['cities-hun'].stringValue;

        return restaurantQuery(city)
            .then((restaurants) => {

                return responseGenerator(
                    restaurants,
                    5,
                    `Can not find any restaurants in ${city}`,
                    'I found these restaurants for you',
                    'More restaurants',
                    `/api/find-restaurant/${city}`,
                    restaurantTemplate
                );

            });

    },
    searchProductByTag: (parameters) => {

        parameters = struct.decode(parameters);

        const tags = parameters['tag'];
        const food = parameters['food'];
        const drink = parameters['drink'];
        const other = parameters['other'];

        return productQuery(tags, food, drink, other)
            .then((products) => {

                return responseGenerator(
                    products,
                    5,
                    `Can not find any products with your given criteria`,
                    'I found these products for you',
                    'More products',
                    `/find-product/`,
                    productTemplate
                );

            });

    },
    lookingForFoodWhere: (parameters) => {

        const city = parameters.fields['city'].stringValue;
        const food = parameters.fields['food'].stringValue.charAt(0).toUpperCase() + parameters.fields['food'].stringValue.slice(1);

        return restaurantQuery(city, food)
            .then((restaurants) => {

                return responseGenerator(
                    restaurants,
                    5,
                    `Sorry, but I can not find any restaurants with ${food} on their menu in ${city}.`,
                    'I found these restaurants for you',
                    'More restaurants',
                    `/api/find-restaurant/${city}/${food}`,
                    restaurantTemplate
                );

            });

    },
    chatbotRegistration: (parameters) => {

        const firstname = parameters['firstname'];
        const lastname = parameters['lastname'];
        const email = parameters['email'];
        const password = parameters['password'];
        const gender = parameters['gender'].toLowerCase();
        const age = String(parameters['age']);
        const avatar = parameters['avatar'];

        if(
            !validator.isEmpty(firstname) &&
            !validator.isEmpty(lastname) &&
            !validator.isEmpty(email) &&
             validator.isEmail(email) &&
            !validator.isEmpty(password) &&
            !validator.isEmpty(gender) &&
             validator.isIn(gender, ['male', 'female', 'other']) &&
            !validator.isEmpty(age) &&
             validator.isInt(age) &&
            !validator.isEmpty(avatar)
        ){

            validator.escape(firstname);
            validator.escape(lastname);
            validator.trim(firstname);
            validator.trim(lastname);

            return User.create({
                firstName: firstname,
                lastName: lastname,
                email: email,
                gender: gender,
                age: age,
                avatar: avatar,
                password: password,
                role: User.getRoles()['user']
            })
            .then(user => {
                console.log("User's auto-generated ID:", user.id);
                return 'Your registration was successful! You can log in and change your profile data on our website\'s Settings tab.';
            })
            .catch(errors => {
                return 'I am sorry, but there was an error in the database. My developers are working on it. Please try again later.';
            });

        }else{
            return 'I am very sorry, but there is an error in one of the fields you provided for the registration. Please check your answers and try again.';
        }

    },
    chatbotFileAuthentication: (parameters, handler, fullQuery) => {

        if(!fullQuery){
            return;
        }

        const email = parameters['email'];
        const password = parameters['password'];

        const contexts = fullQuery.outputContexts;

        return User.findOne({ where: {email: email} })
            .then(userResult => {

                return userResult.asyncValidatePassword(password)
                    .then((result) => {

                        if(result){

                            const textResponse = fullQuery.fulfillmentText;

                            handler.createContext(getContextIdFromContextName(contexts[1].name), {email: email}, 3);

                            return textResponse;
                        }else{
                            resetAuthentication(handler, contexts);

                            return 'Your password was incorrect. Please try again.';
                        }

                    })
                    .catch((err) => {
                        console.log(err);
                    });

            }).catch(err => {
                console.log(err);

                resetAuthentication(handler, contexts);

                return 'Your email address was incorrect. Please try again! What is your email address?';
            });

    },
    chooseRestaurant: (parameters, handler, fullQuery) => {
        const email = parameters['email'];

        const contexts = fullQuery.outputContexts;

        return User.findOne({ where: {email: email}, include: 'restaurants'})
            .then((user) => {

                let messageText = '';

                if(user.get().restaurants.length > 0){

                    messageText = randomResponse([
                        'Alright. Now please choose the number of the restaurant where you want to add the document:\n',
                        'Ok. Now select the number of the restaurant where you want to add the new file:\n',
                        'Thank you. Please choose a restaurant where you want to add the document:\n'
                    ]);

                    user.get().restaurants.map((restaurant, index) => {
                        messageText += `   ${index + 1}. ${restaurant.name} \n`;
                    });
                }else{
                    messageText = 'I am sorry, but you do not have any restaurant. You can add a new restaurant on our site in the Add Restaurant tab.';
                }

                //delete the first context to prevent wrong intent match
                handler.deleteContext(getContextIdFromContextName(contexts[contexts.length - 2].name));

                return messageText;
            })
            .catch((err) => {

                console.log(err);

                resetAuthentication(handler, contexts);

                return 'Your email address was incorrect. Please try again! What is your email address?';
            });

    },
    addRestaurantMenu: (parameters, handler, fullQuery) => {
        const email = parameters['email'];
        const document = parameters['document'];
        const index = parameters['index'];

        const contexts = fullQuery.outputContexts;

        return User.findOne({ where: {email: email}, include: 'restaurants'})
            .then((user) => {

                return user.get().restaurants[index - 1].update({
                    menuFile: document
                })
                .then(() => {

                    contexts.map((context) => {
                        handler.deleteContext(getContextIdFromContextName(context.name));
                    });

                    return 'Document successfully added to your restaurant!';
                })
                .catch((err) => {
                    console.log(err);
                });

            })
            .catch((err) => {
                console.log(err);
            });

    },
    addRestaurantDescription: (parameters, handler, fullQuery) => {
        const email = parameters['email'];
        const document = parameters['document'];
        const index = parameters['index'];

        const contexts = fullQuery.outputContexts;

        return User.findOne({ where: {email: email}, include: 'restaurants'})
            .then((user) => {

                return user.get().restaurants[index - 1].update({
                    descriptionFile: document
                })
                    .then(() => {

                        contexts.map((context) => {
                            handler.deleteContext(getContextIdFromContextName(context.name));
                        });

                        return 'Document successfully added to your restaurant!';
                    })
                    .catch((err) => {
                        console.log(err);
                    });

            })
            .catch((err) => {
                console.log(err);
            });

    }
};

export default queryHandlers;