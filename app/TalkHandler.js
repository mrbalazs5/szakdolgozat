import DialogFlow from './Dialogflow';
import queryHandlers from './QueryHandlers';
import { struct } from 'pb-util';

//Handles user input, sends data to the NLP API
class TalkHandler{

    constructor(){

        this.handler = new DialogFlow(process.env.DIALOGFLOW_PROJECT_ID, 'en-US');
        this.intents = {
            welcome: {responseType: 'text'},
            bye: {responseType: 'text'},
            unknown: {responseType: 'text'},
            hungry: {responseType: 'text'},
            hungry_where: {responseType: 'query', handler: 'hungryWhere'},
            restaurantSomewhere: {responseType: 'query'},
            wants_to_eat: {responseType: 'text'},
            doesnt_wants_to_eat: {responseType: 'text'},
            looking_for_restaurant: {responseType: 'query', handler: 'hungryWhere'},
            looking_for_food: {responseType: 'text'},
            looking_for_food_where: {responseType: 'payloadQuery', handler: 'lookingForFoodWhere'},
            looking_for_products_by_tag: {responseType: 'query', handler: 'searchProductByTag'},
            //user registration
            registration_start: {responseType: 'text'},
            reg_firstname: {responseType: 'text'},
            reg_lastname: {responseType: 'text'},
            reg_email: {responseType: 'object', options: ['hideInput']},
            reg_password: {responseType: 'text'},
            reg_gender: {responseType: 'text'},
            reg_age: {responseType: 'text'},
            reg_avatar: {responseType: 'contextQuery', handler: 'chatbotRegistration'},
            //restaurant intents
            res_description_email: {responseType: 'object', options: ['hideInput']},
            res_menu_email: {responseType: 'object', options: ['hideInput']},
            res_password: {responseType: 'contextQuery', options: ['withAuth'], handler: 'chatbotFileAuthentication'},
            res_add_description_start: {responseType: 'text'},
            res_add_menu_start: {responseType: 'text'},
            res_add_menu_upload: {responseType: 'contextQuery', options: ['withAuth'], handler: 'chooseRestaurant'},
            res_add_description_upload: {responseType: 'contextQuery', options: ['withAuth'], handler: 'chooseRestaurant'},
            res_add_menu_end: {responseType: 'contextQuery', options: ['withAuth'], handler: 'addRestaurantMenu'},
            res_add_description_end: {responseType: 'contextQuery', options: ['withAuth'], handler: 'addRestaurantDescription'},

        };

        this.queryHandlers = queryHandlers;

    }

    //sends a 'think' to the NLP API which creates a specific context
    think(to){
        return Promise.resolve(
            this.handler.sendRequest({
                text: {
                    text: to,
                    languageCode: this.handler.language,
                }
            })
        );
    }

    //sends the user's message to the NLP API and returns an answer
    //which can be a simple text or a complex query
    listen(message){

        return new Promise((resolve, reject) => {

            resolve(
                this.handler.getSingleResponse(message)
                    .then((response) => {

                        const intent = this.intents[response.action] ? this.intents[response.action] : {responseType: 'text'};

                        console.log(response);

                        switch (intent.responseType) {
                            //for simple intents
                            case 'text':

                                return response.fulfillmentText;
                            //for intents which require a specific message transformation
                            case 'object':

                                let responseObject = {text: response.fulfillmentText}

                                if(intent.options.includes('hideInput')){
                                    responseObject['hideInput'] = true;
                                }

                                return responseObject;
                            //for intents which require a DB query
                            case 'query':

                                return this.queryHandlers[intent.handler](response.parameters);
                            //for intents which require a DB query, the parameters comes from a given payload
                            case 'payloadQuery':

                                const parameters = response.fulfillmentMessages[0].payload;

                                return this.queryHandlers[intent.handler](parameters);
                            //for intents which require a DB query, the parameters comes from the context
                            case 'contextQuery':

                                let contextParameters = {};

                                response.outputContexts.map((context) => {
                                    contextParameters = {...contextParameters, ...struct.decode(context.parameters)}
                                });

                                let contextResponse = '';

                                if(intent.options && intent.options.includes('withAuth')){
                                    contextResponse = this.queryHandlers[intent.handler](contextParameters, this.handler, response);
                                }else{
                                    contextResponse = this.queryHandlers[intent.handler](contextParameters);
                                }

                                return contextResponse;

                            default:

                                return response.fulfillmentText;

                        }

                    })
            );

        });


    }

}

export default TalkHandler;
