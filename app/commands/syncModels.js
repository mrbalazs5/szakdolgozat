require('dotenv').config();
import DialogFlow from "../Dialogflow";
const db = require('../../models/index.js');

const df = new DialogFlow(process.env.DIALOGFLOW_PROJECT_ID, 'en-US');

df.truncateEntity('food');
df.truncateEntity('drink');
df.truncateEntity('other_product');
df.truncateEntity('tag');

db.sequelize.sync({force: true})
    .catch((error) => {
        console.log(error);
    });

