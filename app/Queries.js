const db = require('../models/index.js');
const Op = db.Sequelize.Op;
const models = require('../models');
const Restaurant = models.Restaurant;
const Product = models.Product;

//this file contains some generally usable database queries

//query for restaurants
export const restaurantQuery = (city, food = 'undefined') => {

    let foodQuery = {};

    if(food !== 'undefined'){
        foodQuery =   {
            association: 'products',
            where: {name: {[Op.like]: food}}
        };
    }else{
        foodQuery = 'products';
    }

    return Restaurant.findAll({
        include: [
            {
                association: 'address',
                include: [
                    'country',
                    {
                        association: 'city',
                        where: {name: {[Op.like]: city}}
                    }
                ]
            },
            {
                association: 'menu',
                include: [
                    foodQuery
                ]
            },
            'contacts'
        ]
    })
    .then((restaurants) => {

        let filteredRestaurants = [];

        restaurants.map((restaurant) => {

            if(food !== 'undefined'){
                if(restaurant.get().address && restaurant.get().menu && restaurant.get().menu.get().products){
                    filteredRestaurants.push(restaurant);
                }
            }else{
                if(restaurant.get().address){
                    filteredRestaurants.push(restaurant);
                }
            }

        });

        return filteredRestaurants;

    }).catch((err) => {
        throw new Error(err);
    });

};

//query for products
export const productQuery = (tags, food = 'undefined', drink = 'undefined', other = 'undefined') => {

    const productTypes = Product.getTypes();

    let foodQuery = {};
    let drinkQuery = {};
    let otherQuery = {};

    if(food !== 'undefined'){
        foodQuery = {
            where: {type: productTypes['food']}
        };
    }

    if(drink !== 'undefined'){
        drinkQuery = {
            where: {type: productTypes['drink']}
        };
    }

    if(other !== 'undefined'){
        otherQuery = {
            where: {type: productTypes['other']}
        };
    }

    return Product.findAll({
        foodQuery,
        drinkQuery,
        otherQuery,
        include: [{
            association: 'tags',
            where: {name: {[Op.in]: tags}}
        }]
    })
        .then((products) => {

            return products;

        }).catch((err) => {
            throw new Error(err);
        });
};