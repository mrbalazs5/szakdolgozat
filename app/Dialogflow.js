import TalkAPI from './TalkAPI';
import df from 'dialogflow';
import appRoot from 'app-root-path';
import { struct } from 'pb-util';

//Dialogflow NLP API
class Dialogflow extends TalkAPI{

    constructor(projectId, language){
        super(df, language);
        this.projectId = projectId;
        this.credentialsPath = appRoot + "/app/credentials/credentials.json";
        this.credentials = require(this.credentialsPath);
    }

    //sends a request to Dialogflow and returns an intent based on the query
    async sendRequest(query){
        const sessionClient = new this.apiObject.SessionsClient({ keyFilename: this.credentialsPath });
        const sessionPath = sessionClient.sessionPath(this.projectId, this.sessionId);

        const request = {
            session: sessionPath,
            queryInput: query,
        };

        try{
            return await sessionClient.detectIntent(request);
        }catch (e) {
            console.log(e);
            return false;
        }

    }

    //lists all active contexts
    async listContexts() {

        const contextsClient = new this.apiObject.ContextsClient({ keyFilename: this.credentialsPath });

        const sessionPath = contextsClient.sessionPath(this.projectId, this.sessionId);

        const request = {
            parent: sessionPath,
        };

        const [response] = await contextsClient.listContexts(request);

        response.forEach(context => {
            console.log(`Context name: ${context.name}`);
            console.log(`Lifespan count: ${context.lifespanCount}`);
        });

    }

    //creates the given context with the given lifespan
    async createContext(contextId, parameters, lifespanCount) {

        const contextsClient = new this.apiObject.ContextsClient({ keyFilename: this.credentialsPath });

        const sessionPath = contextsClient.sessionPath(this.projectId, this.sessionId);
        const contextPath = contextsClient.contextPath(
            this.projectId,
            this.sessionId,
            contextId
        );

        const createContextRequest = {
            parent: sessionPath,
            context: {
                name: contextPath,
                parameters: struct.encode(parameters),
                lifespanCount: lifespanCount,
            },
        };

        const responses = await contextsClient.createContext(createContextRequest);
        console.log(`Created ${responses[0].name} context`);
    }

    //deletes the given context
    async deleteContext(contextId) {

        const contextsClient = new this.apiObject.ContextsClient({ keyFilename: this.credentialsPath });

        const contextPath = contextsClient.contextPath(
            this.projectId,
            this.sessionId,
            contextId
        );

        const request = {
            name: contextPath,
        };

        const result = await contextsClient.deleteContext(request);
        console.log(`Context ${contextPath} deleted`);
        return result;

    }

    //truncates the given Entity table on Dialogflow
    truncateEntity(entityName){

        const entitiesClient = new this.apiObject.EntityTypesClient({
            credentials: this.credentials,
        });

        const projectId = this.projectId;
        const agentPath = entitiesClient.projectAgentPath(projectId);

        entitiesClient
            .listEntityTypes({parent: agentPath})
            .then((responses) => {
                const resources = responses[0];
                for (let i = 0; i < resources.length; i++) {
                    const entity = resources[i];
                    if (entity.displayName === entityName) {
                        return entity;
                    }
                }
                throw new Error();

            })
            .then((entity) => {

                const updatedEntityList = [];

                entity.entities = updatedEntityList;

                const request = {
                    entityType: entity,
                    updateMask: {
                        paths: ['entities'],
                    },
                };

                return entitiesClient.updateEntityType(request);

            })
            .then((responses) => {

                console.log('Truncated entity type:', JSON.stringify(responses[0]));

            })
            .catch((err) => {

                if (err instanceof Error) {
                    console.error('Could not find the entity.');
                    return;
                }
                console.error('Error updating entity type:', err);

            });

    }

    //adds a new entity to the given Entity table
    addEntities(entityName, values) {

        const entitiesClient = new this.apiObject.EntityTypesClient({
            credentials: this.credentials,
        });

        const projectId = this.projectId;
        const agentPath = entitiesClient.projectAgentPath(projectId);

        entitiesClient
            .listEntityTypes({parent: agentPath})
            .then((responses) => {
                const resources = responses[0];
                for (let i = 0; i < resources.length; i++) {
                    const entity = resources[i];
                    if (entity.displayName === entityName) {
                        return entity;
                    }
                }
                throw new Error();

            })
            .then((entity) => {

                const updatedEntityList = [
                    ...entity.entities,
                    ...values
                ];

                entity.entities = updatedEntityList;

                const request = {
                    entityType: entity,
                    updateMask: {
                        paths: ['entities'],
                    },
                };

                return entitiesClient.updateEntityType(request);

            })
            .then((responses) => {

                console.log('Updated entity type:', JSON.stringify(responses[0]));

            })
            .catch((err) => {

                if (err instanceof Error) {
                    console.error('Could not find the entity.');
                    return;
                }
                console.error('Error updating entity type:', err);

            });

    }

    //get a single(first) response from the API
    getSingleResponse(message){

        return new Promise((resolve, reject) => {

            resolve(
                this.sendRequest({
                    text: {
                        text: message,
                        languageCode: this.language,
                    }
                }).then((responses) => {
                    return responses[0].queryResult;
                })
            );

        });

    }

}

export default Dialogflow;