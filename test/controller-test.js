import chai from 'chai';
const chaiHttp = require('chai-http');
import server from '../server.js';
let should = chai.should();

chai.use(chaiHttp);

describe('Get cities', function() {
    it('should list ALL cities on /api/get-cities GET', function(done) {

        chai.request(server)
            .get('/api/get-cities')
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                console.log(res.body);
                done();
            });
    });
});

describe('Get ingredients', function() {
    it('should list ALL ingredients on /api/get-ingredients GET', function(done) {

        chai.request(server)
            .get('/api/get-ingredients')
            .end(function(err, res){
                res.should.have.status(200);
                console.log(res.body);
                done();
            });
    });
});

describe('Find restaurant by city', function() {
    it('should list all restaurants in the given city on /api/find-restaurant/Szentendre GET', function(done) {

        chai.request(server)
            .get('/api/find-restaurant/Szentendre')
            .end(function(err, res){
                res.should.have.status(200);
                console.log(res.body);
                done();
            });
    });
});

// describe('Get restaurants of the logged in user', function() {
//     it('should list all restaurants of the logged in user on /api/get-restaurants GET', function(done) {
//
//         chai.request(server)
//             .post('/api/profile/sign-in')
//             .send({
//                 email: 'admin@t.com',
//                 password: 'admin'
//             })
//             .end(function(err1, res1){
//                 chai.request(server)
//                     .get('/api/get-restaurants')
//                     .end(function(err2, res2){
//                         res2.should.have.status(200);
//                         console.log(res2.body);
//                         done();
//                     });
//             });
//     });
// });

// describe('Register user', function() {
//     it('should register a new user on /api/profile/sign-up POST', function(done) {
//
//         chai.request(server)
//             .post('/api/profile/sign-up')
//             .send({firstName: 'Testfn',
//                 lastName: 'Testln',
//                 email: 'test@gmail.com',
//                 gender: 'male',
//                 avatar: '/my/avatar',
//                 age: '20',
//                 password: 'testpass'
//             })
//             .end(function(err, res){
//                 res.should.have.status(200);
//                 console.log(res.body);
//                 done();
//             });
//     });
// });

describe('Login user', function() {
    it('should login a user on /api/profile/sign-in POST', function(done) {

        chai.request(server)
            .post('/api/profile/sign-in')
            .send({
                email: 'admin@t.com',
                password: 'admin'
            })
            .end(function(err, res){
                res.should.have.status(200);
                console.log(res.body);
                done();
            });
    });
});

// describe('Delete user', function() {
//     it('should delete a user on /api/delete-user/8 DELETE', function(done) {
//
//         chai.request(server)
//             .delete('/api/delete-user/8')
//             .end(function(err, res){
//                 res.should.have.status(200);
//                 console.log(res.body);
//                 done();
//             });
//     });
// });

// describe('Create restaurant', function() {
//     it('should create a new restaurant on /api/restaurant/add-restaurant POST', function(done) {
//
//         chai.request(server)
//             .post('/api/restaurant/add-restaurant')
//             .send({
//                 name: 'Test Restaurant',
//                 logo: '/my/logo',
//                 website: 'https://google.com',
//                 country: 'Hungary',
//                 city: 'Szentendre',
//                 district: 'Test District',
//                 street: 'Test Street',
//                 latitude: '0.01',
//                 longitude: '0.01',
//                 contacts: [{
//                     name: 'Test',
//                     phoneNumber: 'test',
//                     email: 'test@tst.com'
//                 }],
//                 user: 'admin@t.com',
//                 descriptionFile: '/my/desc',
//                 menuFile: '/my/menu',
//             })
//             .end(function(err, res){
//                 res.should.have.status(200);
//                 console.log(res.body);
//                 done();
//             });
//     });
// });

// describe('Delete restaurant', function() {
//     it('should delete a restaurant on /api/restaurant/delete-restaurant/3 DELETE', function(done) {
//
//         chai.request(server)
//             .delete('/api/restaurant/delete-restaurant/3')
//             .end(function(err, res){
//                 res.should.have.status(200);
//                 console.log(res.body);
//                 done();
//             });
//
//     });
// });