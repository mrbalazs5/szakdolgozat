import chai from "chai";
import mapMessageTypeToString from "../client/src/utils/mapMessageTypeToString";
import dateFormatter from "../client/src/utils/dateFormatter";

describe('Format a given date', function() {
    it('should return a formatted date', function() {

        const dateString = dateFormatter(new Date(), 'full');

        console.log(dateString);

        chai.expect(dateString).to.be.a('string');
    });
});

describe('Map message type to string', function() {
    it('should return a string based on the message\'s type', function() {

        let messageType = 0;

        let messageTypeString = mapMessageTypeToString(messageType);

        console.log(messageTypeString);

        chai.expect(messageTypeString).to.be.a('string');
        chai.expect(messageTypeString).to.be.equal('client');

        messageType = 1;

        messageTypeString = mapMessageTypeToString(messageType);

        console.log(messageTypeString);

        chai.expect(messageTypeString).to.be.a('string');
        chai.expect(messageTypeString).to.be.equal('bot');
    });
});