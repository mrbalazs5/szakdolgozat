import TalkHandler from "../app/TalkHandler";
require('dotenv').config();
import chai from "chai";
import server from "../server";
import randomResponse from '../utils/randomResponse';
import fileHandler from '../utils/fileHandler';
import Dialogflow from '../app/Dialogflow';

describe('Get random response', function() {
    it('should return a random response', function() {
        chai.expect(randomResponse(['test1', 'test2'])).to.be.a('string');
    });
});

describe('Dialogflow send query', function() {
    it('should return a response from Dialogflow', function() {

        const df = new Dialogflow(process.env.DIALOGFLOW_PROJECT_ID, 'en-US');

        const message = 'Hi';

        return df.sendRequest({
            text: {
                text: message,
                languageCode: df.language,
            }
        })
        .then((responses) => {
            console.log(responses);
            chai.expect(responses).to.be.an('array');
            chai.expect(responses[0].queryResult).to.be.an('object');
        })
        .catch((err) => {
            console.log(err);
        });

    });
});

describe('Dialogflow get single response', function() {

    it('should get a single response from Dialogflow', function () {

        const df = new Dialogflow(process.env.DIALOGFLOW_PROJECT_ID, 'en-US');

        const message = 'Hi';

        return df.getSingleResponse(message)
            .then((response) => {

                console.log(response);

                chai.expect(response).to.be.an('object');

            }).catch((err) => {
                console.log(err);
            });

    });


});

describe('Delete file', function() {
    it('should delete a file', function() {

        const filePath = 'client/public/uploads/test.txt';

        //create a test file to delete it later
        chai.expect(fileHandler.create(filePath, 'Test content')).to.be.equal(true);

        chai.expect(fileHandler.delete(filePath, function () {
            console.log('test');
        })).to.be.equal(true);

    });
});

describe('TalkHandler listen', function() {

    it('should return a message from Dialogflow based on the user\'s input', function () {

        const th = new TalkHandler();

        const message = 'Hi';

        return th.listen(message)
            .then((res) => {
                console.log(res);

                chai.expect(res).to.be.a('string');
            })
            .catch((err) => {
                console.log(err);
            });

    });


});