export const acceptedImageMimeTypes = [
    'image/jpeg',
    'image/jpg',
    'image/png',
    '',
    'image/svg+xml'
];

export const acceptedDocumentMimeTypes = [
    'application/x-abiword',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text',
    'application/pdf',
    'text/plain'
];