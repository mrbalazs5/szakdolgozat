import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export default function withAuth(ComponentToProtect, role) {
        return class extends Component {

            constructor() {
                super();
                this.state = {
                    loading: true,
                    redirect: false,
                };
            }

            componentDidMount() {
                fetch('/api/checkToken')
                    .then(res => {
                        if (res.status === 200) {
                            return res.json()
                        } else {
                            const error = new Error(res.error);
                            throw error;
                        }
                    })
                    .then(res => {
                        if(res.user.role === role || res.user.role === 1){
                            this.setState({ loading: false });
                        }
                        else {
                            const error = new Error(res.error);
                            throw error;
                        }

                    })
                    .catch(err => {
                        this.setState({ loading: false, redirect: true });
                    });
            }

            render() {
                const { loading, redirect } = this.state;
                if (loading) {
                    return null;
                }
                if (redirect) {
                    //TODO: admin redirect
                    return <Redirect to={{
                        pathname : '/profile/sign-in',
                        state : {flashMessage: {warning: ['Please sign in to access this page']}}
                    }} />;
                }
                return (
                    <React.Fragment>
                        <ComponentToProtect {...this.props} />
                    </React.Fragment>
                );
            }

    }
}