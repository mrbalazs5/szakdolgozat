const dateFormatter = (date, format) => {

    let fullDate = new Date(date);
    let month = fullDate.toLocaleString('en-GB', { month: 'long'});
    let day = fullDate.toLocaleString('en-GB', { day: '2-digit'});
    let year = fullDate.toLocaleString('en-GB', { year: 'numeric'});
    let hour = fullDate.toLocaleString('en-GB', { hour: 'numeric'});
    let minute = fullDate.toLocaleString('en-GB', { minute: 'numeric'});

    switch (format){
        case 'full':
            return `${hour}:${minute} ${month} ${day}, ${year}`;
        default:
            return `${hour}:${minute} ${month} ${day}, ${year}`;
    }
};

export default dateFormatter;