const mapMessageTypeToString = (messageType) => {

    let messageTypeString = '';

    switch(parseInt(messageType)){
        case 0:
            messageTypeString = 'client';
            break;
        case 1:
            messageTypeString = 'bot';
            break;
        default:
            messageTypeString = 'bot';
    }

    return messageTypeString;

};

export default mapMessageTypeToString;