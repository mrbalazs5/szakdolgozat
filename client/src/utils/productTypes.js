const productTypes = {
    'Food': 0,
    'Drink': 1,
    'Other': 2
};

export default productTypes;