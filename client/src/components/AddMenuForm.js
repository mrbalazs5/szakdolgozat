import React from 'react';
import './AddMenuForm.scss';
import SimpleReactValidator from 'simple-react-validator';
import classNames from 'classnames';
import SVGIcon from "./SVGIcon";
import {acceptedImageMimeTypes} from "../utils/acceptedMimeTypes";
import productTypes from "../utils/productTypes";
import defaultLogo from '../img/default-restaurant.png';
import {confirmAlert} from "react-confirm-alert";

class AddMenuForm extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            //fields
            products: [],
            //helper states
            restaurants: [],
            ingredients: [],
            tag: {
                tagName: ''
            },
            product: {
                restaurant: '',
                productName: '',
                productType: '0',
                productPrice: '',
                productImage: '',
                ingredients: [],
                tags: []
            },
        };

        this.addProductRef = React.createRef();

        this.validator = new SimpleReactValidator({
            validators: {
                imageIsValid: {
                    message: 'Avatar \'s image type must be png, jpeg or svg',
                    rule: (val, params, validator) => {
                        return acceptedImageMimeTypes.includes(val.type);
                    }
                }
            }
        });

        this.handleProductChange = this.handleProductChange.bind(this);
        this.handleTagChange = this.handleTagChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.addProductHandler = this.addProductHandler.bind(this);
        this.addTagHandler = this.addTagHandler.bind(this);
        this.imageButtonHandler = this.imageButtonHandler.bind(this);
        this.handleIngredientChange = this.handleIngredientChange.bind(this);
        this.handleRestaurantClick = this.handleRestaurantClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetProduct = this.resetProduct.bind(this);
        this.wantsToDeleteProductExistsHandler = this.wantsToDeleteProductExistsHandler.bind(this);
        this.wantsToDeleteProductHandler = this.wantsToDeleteProductHandler.bind(this);
        this.changeRestaurant = this.changeRestaurant.bind(this);
    }

    componentDidMount(){

        fetch('/api/get-restaurants')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((restaurants) => {

                restaurants.forEach((restaurant) => {
                    restaurant['active'] = false;
                });

                this.setState({
                    restaurants: restaurants
                });

            })
            .catch(err => {
                console.log(err);
            });

        fetch('/api/get-ingredients')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((ingredients) => {

                let resIngredients = {};

                ingredients.forEach((ingredient) => {
                    resIngredients[ingredient.name] = false;
                });

                let product = Object.assign({}, this.state.product);
                product['ingredients'] = resIngredients;


                this.setState({
                    ingredients: resIngredients,
                    product: product
                });

            })
            .catch(err => {
                console.log(err);
            });

    }

    handleProductChange(e){

        let name = e.target.name;
        let value = e.target.value;

        let product = Object.assign({}, this.state.product);
        product[name] = value;

        this.setState({
            product: product
        });

    }

    handleTagChange(e){

        let name = e.target.name;
        let value = e.target.value;

        let tag = Object.assign({}, this.state.tag);
        tag[name] = value;
        this.setState({
            tag: tag
        });

    }

    handleImageChange(e){
        let input = e.target;

        if (input.files && input.files[0]) {

            let product = Object.assign({}, this.state.product);
            product['productImage'] = input.files[0];

            this.setState({
                product: product
            });

        }

    }

    addProductHandler(){

        if(
            this.validator.fieldValid('productName') &&
            this.validator.fieldValid('productImage') &&
            this.validator.fieldValid('productType') &&
            this.validator.fieldValid('productPrice')
        ){
            this.setState({
                products: [...this.state.products, this.state.product]
            }, () => {

                let product = Object.assign({}, this.state.product);
                this.resetProduct(product);

                this.setState({
                    product: product
                });

            });
        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    addTagHandler(){

        if(
            this.validator.fieldValid('tagName')
        ){

            let product = Object.assign({}, this.state.product);
            product['tags'] = [...product['tags'], this.state.tag];

            this.setState({
                product: product,
                tag: {
                    tagName: ''
                }
            }, () => {
                document.getElementById('tagName').value = '';
            });
        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    imageButtonHandler(){
        document.getElementById('productImage').click();
    }

    handleIngredientChange(e){
        let name = e.target.name;
        let checked = e.target.checked;

        let ingredients = Object.assign({}, this.state.product.ingredients);
        ingredients[name] = checked;
        let product = Object.assign({}, this.state.product);

        product['ingredients'] = ingredients;

        this.setState({
            product: product
        });

    }

    resetProduct(product){
        product['productName'] = '';
        product['productType'] = 0;
        product['productPrice'] = '';
        product['productImage'] = '';
        product['tags'] = '';
        product['ingredients'] = this.state.ingredients;
        product['restaurant'] = '';

        document.getElementById('productName').value = '';
        document.getElementById('productImage').value = '';
        document.getElementById('productType').value = '';
        document.getElementById('productPrice').value = '';

        Array.from(document.getElementsByClassName('ingredient-checkbox')).forEach((ingredientCheckbox) => {
            ingredientCheckbox.checked = false;
        });

        return product;
    }

    handleRestaurantClick(index){
        let restaurants = this.state.restaurants;
        let product = Object.assign({}, this.state.product);

        restaurants.forEach((restaurant) => {
            restaurant.active = false;
        });

        restaurants[index].active = !restaurants[index].active;

        product = this.resetProduct(product);
        product['restaurant'] = restaurants[index];

        this.setState({
            product: product,
            restaurants: restaurants
        }, () => {
            window.scrollTo(0, this.addProductRef.current.offsetTop - 77);
        });

    }

    changeRestaurant(restaurantInput){

        let product = this.state.product;

        product['restaurant'] = restaurantInput;

        this.setState({
            product: product
        });
    }

    deleteProductHandler(product,existsInDB){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure you want to delete this product?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => existsInDB ? this.wantsToDeleteProductExistsHandler(product) : this.wantsToDeleteProductHandler(product)
                },
                {
                    label: 'No',
                }
            ]
        });
    }

    wantsToDeleteProductHandler(product){
        this.setState({
            products: this.state.products.filter((prod) => {
                return prod.id !== product.id
            })
        });
    }

    wantsToDeleteProductExistsHandler(product){
        let restaurant = this.state.product.restaurant;

        fetch(`/api/restaurant/delete-product/${product.id}`, {
            method: 'DELETE'
        })
            .then(() => {

                restaurant.menu.products = restaurant.menu.products.filter((prod => {
                    return prod.id !== product.id;
                }));

                this.changeRestaurant(restaurant);

            })
            .catch((err) => {
                console.log(err);
            });

    }

    handleSubmit(e){
        e.preventDefault();

        if (this.state.products.length > 0) {

            let readyProducts = this.state.products.map((product) => {

                let formData  = new FormData();

                formData.append('image', product.productImage);

                return fetch('/api/uploadImage',{
                    method: 'POST',
                    body: formData
                })
                .then((res) => {
                    return res.json();
                })
                .then((res) => {

                    if(res.filePath){

                        product['productImage'] = res.filePath;

                        return product;

                    }else if(res.errors){
                        return this.props.history.push({
                            pathname: "/add-menu",
                            state: {flashMessage: res}
                        });
                    }

                });

            });

            Promise.all(readyProducts).then((productsData) => {

                this.setState({
                    products: []
                });

                fetch('/api/change-menu', {
                    method: 'POST',
                    body: JSON.stringify({
                        products: productsData
                    }),
                    headers: {"Content-Type": "application/json"}
                })
                .then((response) => {
                    return response.json()
                })
                .then((response) => {

                    if(response){

                        window.location.reload();

                        this.props.history.push({
                            state: {flashMessage: response}
                        });

                    }

                });

            });

        }else{

            this.props.history.push({
                state: {flashMessage: {errors: ['You need to add at least one product.']}}
            });


        }

    }

    render(){

        let restaurant = this.state.product.restaurant ? this.state.product.restaurant : {};
        console.log(restaurant);

        return(
            <form id={'add-menu'} className={'add-menu-form form'} onSubmit={this.handleSubmit}>

                <h1 className={'form-title'}>My menus</h1>

                    <div className={'all-product'}>
                        {
                            !restaurant.menu ? '' : restaurant.menu.products.length === 0 ? '' : (
                                <div className={'products-added'}>
                                    <h2>Previously added products</h2>
                                    {
                                        restaurant.menu.products.map((product, index) => (
                                            <div key={index} className={'product-added'}>
                                                <span>{ product.name }</span>
                                                <span>{ product.price }€</span>
                                                <SVGIcon
                                                    name={'CROSS_ICON'}
                                                    className={'delete-icon'}
                                                    onClick={() => this.deleteProductHandler(product, true)}
                                                />
                                            </div>
                                        ))
                                    }
                                </div>

                            )
                        }

                        {
                            this.state.products.length === 0 ? '' : (

                                <div className={'products-added'}>
                                    <h2>Currently added products</h2>
                                    {
                                        this.state.products.map((product, index) => (
                                            <div key={index} className={'product-added'}>
                                                <span>{ product.productName }</span>
                                                <span>{ product.productPrice }€</span>
                                                <SVGIcon
                                                    name={'CROSS_ICON'}
                                                    className={'delete-icon'}
                                                    onClick={() => this.deleteProductHandler(product, false)}
                                                />
                                            </div>

                                        ))
                                    }
                                </div>
                            )
                        }

                </div>

                <div className={'messages-wrapper'}>
                    <div className={'alert'}>
                        {this.validator.message('restaurant', this.state.product.restaurant, 'required')}
                        {this.validator.message('productName', this.state.product.productName, 'required|string')}
                        {this.validator.message('productImage', this.state.product.productImage, 'required|imageIsValid')}
                        {this.validator.message('productType', this.state.product.productType, 'required|integer')}
                        {this.validator.message('productPrice', this.state.product.productPrice, 'required|integer')}
                        {this.validator.message('tagName', this.state.tag.tagName, 'required|string')}
                    </div>
                </div>

                <div className={'restaurants-wrapper'}>
                    {
                        this.state.restaurants.map((restaurant, index) => (
                            <div key={index}
                                 className={classNames('restaurant', restaurant.active ? 'active' : '')}
                                 onClick={() => { this.handleRestaurantClick(index) }}
                            >
                                <div className={'restaurant-logo'}>
                                    <img src={restaurant.logo ? restaurant.logo : defaultLogo} alt={restaurant.name}/>
                                </div>
                                <div className={'restaurant-data'}>
                                    <h1>{restaurant.name}</h1>
                                    <div>
                                        <span className={'data-title'}>Number of products:</span>
                                        <span>{ restaurant.menu ? restaurant.menu.products.length : '0' }</span>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>

                <div className={'product-wrapper'} ref={this.addProductRef} style={  this.state.product.restaurant ? {display: 'block'} : {display: 'none'} }>

                    <div className={"form-item-wrapper"}>

                        <div className={ classNames("form-item inline") }>
                            <label htmlFor={'productType'}>Product Type</label>
                            <select id={'productType'} value={this.state.product.productType}  name={'productType'} onChange={this.handleProductChange} >
                                {
                                    Object.keys(productTypes).map((keyName, index) => (
                                        <option key={index} value={productTypes[keyName]}>{ keyName }</option>
                                    ))
                                }
                            </select>
                        </div>

                        <div className={ classNames("form-item inline") }>
                            <label htmlFor={'productName'}>Name</label>
                            <input id={'productName'} type="text" name={'productName'} onChange={this.handleProductChange} />
                        </div>

                    </div>

                    <div className={"form-item-wrapper"}>

                        <div className={ classNames("form-item inline") }>
                            <label htmlFor={'productImage'}>Image</label>
                            <div className={'upload-btn'} onClick={this.imageButtonHandler}>
                                <SVGIcon name={'UPLOAD_ICON'} className={'upload-icon'} />
                                Upload Image
                            </div>
                            <input id={'productImage'} type="file" name={'productImage'} onChange={this.handleImageChange} hidden />
                        </div>

                        <div className={ classNames("form-item inline") }>
                            <label htmlFor={'productPrice'}>Price</label>
                            <div className={'price'}>
                                <input id={'productPrice'} type="number" name={'productPrice'} value={this.state.product.productPrice} onChange={this.handleProductChange} />
                                <span>€</span>
                            </div>
                        </div>

                    </div>

                    <h1 className={'row-title'}>Ingredients</h1>

                    <div className={'ingredients-wrapper'}>
                        {

                            Object.keys(this.state.product.ingredients).map((keyName, index) => (

                                <div key={index} className={'checkbox-wrapper'}>
                                   <input className={'ingredient-checkbox'} type="checkbox" name={keyName} onChange={this.handleIngredientChange} value={keyName} />{keyName}
                                </div>

                            ))
                        }
                    </div>

                    <div className={'tags-wrapper'}>

                        <h1 className={'row-title'}>Tags</h1>

                        <div className={'tags'}>
                        {
                            this.state.product.tags ?
                                this.state.product.tags.map((tag, index) => (
                                    <span key={index} className={'tag'}>{ tag.tagName }</span>
                                ))
                            : ''
                        }
                        </div>

                        <div className={ classNames("form-item") }>
                            <label htmlFor={'tagName'}>Name</label>
                            <input id={'tagName'} type="text" name={'tagName'} onChange={this.handleTagChange} />
                            <div className={'field-description'}>
                                Tag's name
                            </div>
                        </div>

                        <div className={'btn-wrapper add-tag-btn'}>
                            <div onClick={this.addTagHandler} className={'submit-btn'}>
                                Add Tag
                            </div>
                        </div>

                    </div>

                    <div className={'btn-wrapper'}>
                        <div onClick={this.addProductHandler} className={'submit-btn'}>
                            Add Product
                        </div>
                    </div>

                </div>

                <div className={'btn-wrapper'}>
                    <div onClick={this.handleSubmit} className={'submit-btn'}>
                        Save Changes
                    </div>
                </div>

            </form>
        );

    }

}


export default AddMenuForm;