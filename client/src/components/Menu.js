import React from 'react';
import './Menu.scss';
import { Link } from 'react-router-dom';
import defaultAvatar from '../img/default-avatar.png';
import classNames from 'classnames';
import ProfileMenu from "./ProfileMenu";

class Menu extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            showProfileMenu: false,
            hasRestaurant: false,
            user: null,
            avatar: defaultAvatar,
            mobileHamburgerOpen: false,
            windowWidth: 0,
        };

        this.handleAvatarClick = this.handleAvatarClick.bind(this);
        this.hamburgerHandler = this.hamburgerHandler.bind(this);
    }

    handleResize = e => {
        const windowWidth = window.innerWidth;

        if(windowWidth >= 768){
            this.setState({
                mobileHamburgerOpen: true
            });
        }else{
            this.setState({
                mobileHamburgerOpen: false
            });
        }

        this.setState({
            windowWidth: windowWidth
        });
    };

    componentDidMount(){

        const windowWidth = window.innerWidth;

        if(windowWidth >= 768){
            this.setState({
                mobileHamburgerOpen: true
            });
        }

        window.addEventListener("resize", this.handleResize);

        fetch('/api/checkToken')
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then(res => {

                if(res.user){

                    this.setState({
                        user: res.user
                    });

                    if(res.user.avatar){
                        this.setState({
                            avatar: res.user.avatar
                        });
                    }

                    if(res.user.hasRestaurant){
                        this.setState({
                            hasRestaurant: true
                        });
                    }

                }

            })
            .catch(err => {
            });

        fetch('/api/get-restaurants')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((restaurants) => {

                this.setState({
                    hasRestaurant: restaurants.length > 0
                });

            })
            .catch(err => {

            });
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResize);
    }

    handleAvatarClick() {

        this.setState(state => ({
            showProfileMenu: !state.showProfileMenu
        }));

    }

    hamburgerHandler(){

        this.setState({
            mobileHamburgerOpen: !this.state.mobileHamburgerOpen
        });

    }

    render(){

        return(
            <div className={'menu'}>
                <div className={classNames('menu-links', !this.state.mobileHamburgerOpen ? 'hide': '')}>
                    <Link to={'/'} className={'menu-link'}>
                        Main Board
                    </Link>
                    {
                        this.state.user ?
                            <Link to={'/add-restaurant'} className={'menu-link'}>
                                Add Restaurant
                            </Link> :
                            ''
                    }

                    {
                        this.state.hasRestaurant ?
                            <Link to={'/add-menu'} className={'menu-link'}>
                                My menus
                            </Link> :
                            ''
                    }

                    <Link to={'/about-us'} className={'menu-link'}>
                        About Us
                    </Link>
                    {/*{*/}
                        {/*this.state.user && this.state.user.role === userRoles['admin'] ?*/}
                            {/*<Link to={'/admin'} className={'menu-link'}>*/}
                                {/*Admin*/}
                            {/*</Link> :*/}
                            {/*''*/}
                    {/*}*/}
                </div>


                <div className={classNames('menu-link', 'profile')}>
                    <img onClick={this.handleAvatarClick} src={ this.state.avatar ? this.state.avatar : defaultAvatar } alt={'Profile'}/>
                    <ProfileMenu user={this.state.user} isOpen={this.state.showProfileMenu} changeMenu={this.handleAvatarClick} />
                    <div className={classNames('hamburger', this.state.mobileHamburgerOpen ? 'open' : '')} onClick={this.hamburgerHandler}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

            </div>
        );

    }

}

export default Menu;