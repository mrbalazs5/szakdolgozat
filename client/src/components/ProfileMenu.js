import React from 'react';
import './ProfileMenu.scss';
import { Link } from 'react-router-dom';
import SVGIcon from "./SVGIcon";
import { Redirect } from 'react-router-dom';
import classNames from 'classnames';

class ProfileMenu extends React.Component {

    constructor(props){

        super(props);
        this.logout = this.logout.bind(this);

        this.state = {
            redirect: false,
            to: null
        };

    }

    logout(){
        fetch('/api/profile/logout',{
            method: 'POST'
        })
            .then((response) => {
                return response.json()
            })
            .then((response) => {
                if(response.info){

                   this.setState({
                       redirect: true,
                       to: <Redirect to={{
                               pathname : '/',
                               state : {flashMessage: response}
                           }} />
                   });

                    window.location.reload();

                }
            });
    }

    render(){

        const { redirect, to } = this.state;

        if(redirect && to){
            return to;
        }

        const menuItems = [
            {icon: 'ADD_USER_ICON', link: '/profile/sign-up', linkName: 'Sign Up', style: null, onClick: ()=>{}, show: !this.props.user},
            {icon: 'LOGIN_ICON', link: '/profile/sign-in', linkName: 'Sign In', style: null, onClick: ()=>{}, show: !this.props.user},
            {icon: 'USER_ICON', link: '/profile', linkName: 'My Profile', style: null, onClick: ()=>{}, show: this.props.user},
            {icon: 'RESTAURANT_ICON', link: '/profile/restaurants', linkName: 'My Restaurants', style: null, onClick: ()=>{}, show: this.props.user},
            {icon: 'SETTINGS_ICON', link: '/profile/settings', linkName: 'Settings', style: null, onClick: ()=>{}, show: this.props.user},
            {icon: 'LOGIN_ICON', link: '#', linkName: 'Logout', style: {transform: 'rotate(180deg)'}, onClick: this.logout, show: this.props.user}
        ]

        return(
            <div className={classNames('profile-menu-wrapper', this.props.isOpen ? 'open' : '') }>
                <ul className={'profile-menu'}>

                    {
                        menuItems.map((menuItem, index) => (
                                menuItem.show ?
                                <Link to={ menuItem.link } key={ index } className={'profile-menu-item'} onClick={ () => { this.props.changeMenu(); menuItem.onClick(); } }>
                                    <SVGIcon name={ menuItem.icon } className={'menu-icon'} style={ menuItem.style }/>
                                    { menuItem.linkName }
                                </Link> :
                                ''
                        ))
                    }

                </ul>
            </div>
        );

    }

}

export default ProfileMenu;