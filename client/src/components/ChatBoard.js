import React from 'react';
import './ChatBoard.scss';
import SVGIcon from "./SVGIcon";
import classNames from 'classnames';
import SimpleReactValidator from 'simple-react-validator';
// import { w3cwebsocket as W3CWebSocket } from "websocket";
import AnimateLoad from './AnimateLoad';
import mapMessageTypeToString from '../utils/mapMessageTypeToString';
import dateFormatter from '../utils/dateFormatter';

import ReconnectingWebSocket from 'reconnecting-websocket';
import {acceptedImageMimeTypes} from "../utils/acceptedMimeTypes";


class ChatBoard extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            messages: [],
            message: '',
            showWait: false,
            hideInput: false,
            image: null,
            docx: null,
            pdf: null
        };

        this.host = window.location.host.replace('www.','');

        this.websocketProtocol = process.env.NODE_ENV === 'production' ? 'wss' : 'ws';

        this.client = new ReconnectingWebSocket(`${this.websocketProtocol}://${this.host}/api/receive-messages`);

        this.validator = new SimpleReactValidator({
            validators: {
                isDOCX: {
                    message: 'The document must have a docx type!',
                    rule: (val, params, validator) => {
                        return [
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                'application/vnd.oasis.opendocument.text'
                               ].includes(val.type);
                    }
                },
                isPDF: {
                    message: 'The document must have a pdf type!',
                    rule: (val, params, validator) => {
                        return val.type === 'application/pdf';
                    }
                },
                imageIsValid: {
                    message: 'Avatar \'s image type must be png, jpeg or svg',
                    rule: (val, params, validator) => {
                        return acceptedImageMimeTypes.includes(val.type);
                    }
                }
            }
        });

        this.handleMessageInput = this.handleMessageInput.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleDocumentChange = this.handleDocumentChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.handleAttachmentClick = this.handleAttachmentClick.bind(this);

    }

    randomNumber = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    stripHtml = (html) => {
        let tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    };

    generateWaitTime = (message) => {
        const waitTime = this.stripHtml(message).replace(/\s/g,'').length * this.randomNumber(50, 150);
        return waitTime < 1500 ? waitTime : 1500;
    };

    componentDidMount() {

        this.client.onopen = () => {

            console.log('WebSocket Client Connected');

            if(this.client.retryCount === 0){

                fetch('/api/get-history')
                    .then((res) => {

                        if (res.status === 200) {

                            return res.json();

                        }
                        else{

                            this.setState({
                                messages: []
                            }, () => {

                                this.client.send(JSON.stringify({message: {firstContact: true}}));
                                return false;

                            });

                        }
                    })
                    .then((history) => {

                        if(history){

                            let historyMessages = [];

                            let historyPromises = history.messages.map((message) => {

                                let date = dateFormatter(message.createdAt, 'full');

                                historyMessages.push({
                                    type: mapMessageTypeToString(message.type),
                                    text: message.text,
                                    date: date
                                });

                            });

                            Promise.all(historyPromises)
                                .then(() => {

                                    this.setState({
                                        messages: historyMessages
                                    }, () => {
                                        this.messagesEnd.scrollIntoView();
                                    });

                                });



                        }

                    })
                    .catch(err => {
                        console.log(err);
                    });

            }

        };

        //bot answer
        this.client.onmessage = (message) => {

            message = JSON.parse(message.data);

            this.setState({showWait : message.wait, hideInput: message.hideInput}, () => {

                this.scrollToBottom();

                let responseTime = this.generateWaitTime(message.text);

                setTimeout(
                    function() {
                        this.setState({ messages: [
                                ...this.state.messages, {type: 'bot', text: message.text, date: dateFormatter(message.date, 'full')}
                            ]
                        }, () => {
                            this.scrollToBottom();
                        });

                        this.setState({showWait : false})

                    }.bind(this),
                    message.wait ? responseTime : 0
                );

            });

        };

        this.client.onclose = () => {
            console.log('WebSocket Client Disconnected');
        };

    }

    handleMessageChange(e){

        this.setState({
            message:  e.target.value
        });

    }

    handleImageChange(e){

        const fieldName = e.target.name;
        const file = e.target.files[0];

        this.setState({
            [fieldName]: file
        }, () => {

            if(this.validator.fieldValid(fieldName)){

                let formData  = new FormData();

                formData.append('image', this.state.image);

                fetch('/api/uploadImage',{
                    method: 'POST',
                    body: formData
                })
                    .then((res) => {
                        return res.json();
                    })
                    .then((res) => {

                        if (res.filePath) {
                            this.sendMessage({image: res.filePath});
                        }

                    });

            }else{
                this.validator.showMessages();
                this.forceUpdate();
            }

        });

    }

    handleDocumentChange(e){

        const fieldName = e.target.name;
        const file = e.target.files[0];

        this.setState({
            [fieldName]: file
        }, () => {

            if(this.validator.fieldValid(fieldName)){
                let formData  = new FormData();

                formData.append('document', this.state[fieldName]);

                fetch('/api/uploadDocument',{
                    method: 'POST',
                    body: formData
                })
                    .then((res) => {
                        return res.json();
                    })
                    .then((res) => {

                        if (res.filePath && res.fileName) {
                            this.sendMessage({document: {name: res.fileName, path: res.filePath}});
                        }

                    });

            }else{
                this.validator.showMessages();
                this.forceUpdate();
            }

        });

    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    handleAttachmentClick(inputId){
        document.getElementById(inputId).click();
    }

    sendMessage(message){

        try{

            let messageObject = {message: message};

            if(this.state.hideInput){
                messageObject['hideInput'] = true;
            }

            this.client.send(JSON.stringify(messageObject));
        }
        catch(error){
            console.log(error);
            return;
        }

        if(message.image){
            message = `<img src="${message.image}"/>`;
        }

        if(message.document){
            const path = message.document.path;
            const name = message.document.name;

            message = `<a target="_blank" class="message-document" href="${path}">${name}<a>`;
        }

        this.setState(
            { messages: [...this.state.messages, ...[
                    {type: 'client', text: this.state.hideInput ? 'HIDDEN MESSAGE' : message, date: dateFormatter(new Date(), 'full')},
                ]
            ],
                hideInput: false
            }, () => {
                this.scrollToBottom();
                document.getElementById('message').value = '';
            }
        );

    }

    handleMessageInput(e){
        e.preventDefault();

        if (this.validator.fieldValid('message')){

           this.sendMessage(this.state.message);

        }else{
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    render(){

        return(
            <div className={'main-board'}>
                <div className={'chat-board'}>

                    <div className={'message-area'}>

                        {
                            this.state.messages.map((message, index) => (
                                <AnimateLoad key={index}>
                                    <div className={'message-wrapper'}>
                                        <div dangerouslySetInnerHTML={{__html: message.text}} title={ message.date } className={classNames('message', message.type === 'bot' ? 'message-bot' : 'message-client') }>
                                        </div>
                                    </div>
                                </AnimateLoad>
                            ))
                        }

                        {
                            this.state.showWait ?

                                <div className="wait">
                                    <div className="rect1"></div>
                                    <div className="rect2"></div>
                                    <div className="rect3"></div>
                                    <div className="rect4"></div>
                                    <div className="rect5"></div>
                                </div> :
                                ''
                        }

                        <div style={{ float:"left", clear: "both" }}
                             ref={(el) => { this.messagesEnd = el; }}>
                        </div>

                    </div>

                    <form className={'message-form'} action={'#'} method={'POST'} onSubmit={ this.handleMessageInput }>

                        <div className={'message-alert'}>
                            {this.validator.message('message', this.state.message, 'required|string')}
                            {this.validator.message('image', this.state.image, 'imageIsValid')}
                            {this.validator.message('docx', this.state.docx, 'isDOCX')}
                            {this.validator.message('pdf', this.state.pdf, 'isPDF')}
                        </div>

                        <div className="message-wrapper">
                            <input
                                type={this.state.hideInput ? 'password' : 'text'}
                                id="message"
                                onChange={ this.handleMessageChange }
                                name={'message'}
                                placeholder="Send a message..."
                                autoComplete="off"
                            />
                            <button className={'send-btn'} type="submit">Send</button>
                        </div>

                        <input type="file"
                               id="image"
                               onChange={ this.handleImageChange }
                               name={'image'}
                               multiple={false}
                               hidden={true}
                               accept="image/png, image/jpeg"
                        />
                        <input type="file"
                               id="docx"
                               onChange={ this.handleDocumentChange }
                               multiple={true}
                               name={'docx'}
                               hidden={true}
                               accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document application/vnd.oasis.opendocument.text"
                        />
                        <input type="file"
                               id="pdf"
                               onChange={ this.handleDocumentChange }
                               multiple={true}
                               name={'pdf'}
                               hidden={true}
                               accept="application/pdf"
                        />
                        <div className={'attachment-wrapper'}>
                            <SVGIcon name={'SEARCH_ICON'} onClick={ this.props.showSearch } className={'icon'} title={'Search chat history'} />
                            <SVGIcon name={'IMAGE_ICON'} onClick={ () => this.handleAttachmentClick('image') } className={'icon'} title={'Send images'} />
                            <SVGIcon name={'DOCX_ICON'} onClick={ () => this.handleAttachmentClick('docx') } className={'icon'} title={'Send DOCX files'} />
                            <SVGIcon name={'PDF_ICON'} onClick={ () => this.handleAttachmentClick('pdf') } className={'icon'} title={'Send PDF files'} />
                        </div>

                    </form>

                </div>
            </div>
        );

    }

}

export default ChatBoard;