import React from 'react';
import './SettingsForm.scss';
import SimpleReactValidator from 'simple-react-validator';
import classNames from "classnames";
import SVGIcon from "./SVGIcon";
import defaultAvatar from '../img/default-avatar.png';
import {acceptedImageMimeTypes} from "../utils/acceptedMimeTypes";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';

class SettingsForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: '',
            avatarImgSrc: '',
            password: '',
            firstName: '',
            lastName: '',
            gender: '',
            age: '',
            avatar: '',
            email: '',
            newPassword: '',
            newPasswordAgain: '',
        };

        this.validator = new SimpleReactValidator({
            validators: {
                passwordMatch: {
                    message: 'Passwords doesn\'t match!',
                    rule: (val, params, validator) => {
                        return val === this.state.newPassword;
                    }
                },
                imageIsValid: {
                    message: 'Avatar \'s image type must be png, jpeg or svg',
                    rule: (val, params, validator) => {
                        return acceptedImageMimeTypes.includes(val.type);
                    }
                }
            }
        });

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.uploadButtonHandler = this.uploadButtonHandler.bind(this);
        this.deleteHandler = this.deleteHandler.bind(this);
        this.wantsToDeleteHandler = this.wantsToDeleteHandler.bind(this);
    }

    uploadButtonHandler(){
        document.getElementById('avatar').click();
    }

    handleChange(e){

        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {[name]: value}
        );

    }

    wantsToDeleteHandler(){

        if (this.validator.allValid()) {

            fetch(`/api/delete-user/${this.state.user.id}`, {
                method: 'DELETE'
            })
                .then((response) => {
                    return response.json()
                })
                .then((response) => {
                    if(response.errors){
                        return this.props.history.push({
                            pathname: "/profile/settings",
                            state: {flashMessage: response}
                        });
                    }else if(response.success){
                        this.props.history.push({
                            pathname: "/",
                            state: {flashMessage: response}
                        });

                        window.location.reload();
                    }
                })
                .catch((err) => {
                    console.log(err);
                });

        }else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    deleteHandler(){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure you want to delete your account?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.wantsToDeleteHandler()
                },
                {
                    label: 'No',
                }
            ]
        });
    };

    handleImageChange(e){
        let input = e.target;

        if (input.files && input.files[0]) {

            if(acceptedImageMimeTypes.includes(input.files[0].type)){

                let reader = new FileReader();

                reader.onload = function(e) {

                    this.setState({
                        avatarImgSrc: e.target.result
                    });

                }.bind(this);

                reader.readAsDataURL(input.files[0]);

            }

            this.setState({
                avatar: input.files[0]
            });

        }

    }

    componentDidMount(){
        fetch('/api/get-user-data')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((user) => {

                this.setState({
                    user: user,
                    avatarImgSrc: user.avatar,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    gender: user.gender,
                    age: user.age,
                    avatar: '',
                    email: user.email,
                    newPassword: '',
                    newPasswordAgain: '',
                })

            })
            .catch(err => {
                console.log(err);
            });
    }



    handleSubmit(e){

        e.preventDefault();

        if (this.validator.allValid()) {

            if(this.state.avatar && this.state.avatar !== this.state.user.avatar){

                let formData  = new FormData();

                formData.append('image', this.state.avatar);

                fetch('/api/uploadImage',{
                    method: 'POST',
                    body: formData
                })
                    .then((res) => {
                        return res.json();
                    })
                    .then((res) => {

                        if(res.filePath){

                            fetch('/api/profile/settings',{
                                method: 'POST',
                                body: JSON.stringify({
                                    userId: this.state.user.id,
                                    firstName: this.state.firstName,
                                    lastName: this.state.lastName,
                                    gender: this.state.gender,
                                    age: this.state.age,
                                    avatar: res.filePath,
                                    email: this.state.email,
                                    password: this.state.password,
                                    newPassword: this.state.newPassword
                                }),
                                headers: {"Content-Type": "application/json"}
                            })
                                .then((response) => {
                                    return response.json()
                                })
                                .then((response) => {

                                    if(response.errors){
                                        return this.props.history.push({
                                            pathname: "/profile/settings",
                                            state: {flashMessage: response}
                                        });
                                    }else if(response.success){

                                        window.location.reload();

                                        return this.props.history.push({
                                            pathname: "/profile/settings",
                                            state: {flashMessage: response}
                                        });
                                    }

                                });

                        }else if(res.errors){
                            return this.props.history.push({
                                pathname: "/profile/settings",
                                state: {flashMessage: res}
                            });
                        }

                    });
            }else{

                fetch('/api/profile/settings',{
                    method: 'POST',
                    body: JSON.stringify({
                        userId: this.state.user.id,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        gender: this.state.gender,
                        age: this.state.age,
                        avatar: this.state.user.avatar,
                        email: this.state.email,
                        password: this.state.password,
                        newPassword: this.state.newPassword
                    }),
                    headers: {"Content-Type": "application/json"}
                })
                    .then((response) => {
                        return response.json()
                    })
                    .then((response) => {

                        if(response.errors){
                            return this.props.history.push({
                                pathname: "/profile/settings",
                                state: {flashMessage: response}
                            });
                        }else if(response.success){
                            return this.props.history.push({
                                pathname: "/profile/settings",
                                state: {flashMessage: response}
                            });
                        }

                    });

            }


        }else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render(){

        return(
            <form id={'settings-form'} className={'settings-form form'} onSubmit={this.handleSubmit}>

                <h1 className={'form-title'}>Change settings</h1>

                <div className={'alert'}>
                    {this.validator.message('firstName', this.state.firstName, 'string')}
                    {this.validator.message('lastName', this.state.lastName, 'string')}
                    {this.validator.message('gender', this.state.gender, 'string|in:female,male,other')}
                    {this.validator.message('age', this.state.age, 'integer|between:3,150,num')}
                    {this.validator.message('avatar', this.state.avatar, 'imageIsValid')}
                    {this.validator.message('email', this.state.email, 'email')}
                    {this.validator.message('password', this.state.password, 'required')}
                    {this.validator.message('newPassword', this.state.newPassword, 'min:6')}
                    {this.validator.message('newPasswordAgain', this.state.newPasswordAgain, 'passwordMatch')}
                </div>

                <div className={'preview'}>
                    <img className={'preview-avatar'}  src={this.state.avatarImgSrc ? this.state.avatarImgSrc : defaultAvatar} alt="Avatar Preview" />
                </div>

                <div className={ classNames("form-item", !this.validator.fieldValid('password') ? 'invalid' : '') }>
                    <label htmlFor={'password'}>Password</label>
                    <input id={'password'} type="password" name={'password'} onChange={this.handleChange} autoComplete="off" />
                    <div className={'field-description'}>
                        Please validate your password first
                    </div>
                </div>

                <div className={"form-item-wrapper"}>
                    <div className={"form-item inline"}>
                        <label htmlFor={'firstName'}>Firstname</label>
                        <input id={'firstName'} value={this.state.firstName} type="text" name={'firstName'} onChange={this.handleChange} />
                    </div>

                    <div className={"form-item inline"}>
                        <label htmlFor={'lastName'}>Lastname</label>
                        <input id={'lastName'} value={this.state.lastName} type="text" name={'lastName'} onChange={this.handleChange} />
                    </div>
                </div>

                <div className="form-item-wrapper">
                    <div className={"form-item inline"}>
                        <label htmlFor={'gender'}>Gender</label>
                        <select id={'gender'} value={this.state.gender} name={'gender'} onChange={this.handleChange} >
                            <option value="female">Female</option>
                            <option value="male">Male</option>
                            <option value="other">Other</option>
                        </select>
                    </div>

                    <div className={"form-item inline"}>
                        <label htmlFor={'age'}>Age</label>
                        <input id={'age'} type="number" name={'age'} value={this.state.age} onChange={this.handleChange} />
                    </div>
                </div>

                <div className={"form-item"}>
                    <label htmlFor={'avatar'}>Avatar</label>
                    <div className={'upload-btn'} onClick={this.uploadButtonHandler}>
                        <SVGIcon name={'UPLOAD_ICON'} className={'upload-icon'} />
                        Upload Avatar
                    </div>
                    <input id={'avatar'} type="file" name={'avatar'} onChange={this.handleImageChange} hidden />
                </div>

                <div className={"form-item"}>
                    <label htmlFor={'email'}>Email</label>
                    <input id={'email'} value={this.state.email} type="email" name={'email'} onChange={this.handleChange} autoComplete="on" />
                </div>

                <div className={"form-item"}>
                    <label htmlFor={'newPassword'}>New password</label>
                    <input id={'newPassword'} type="password" name={'newPassword'} onChange={this.handleChange} autoComplete="off" />
                </div>

                <div className={"form-item"}>
                    <label htmlFor={'newPasswordAgain'}>Verify new password</label>
                    <input id={'newPasswordAgain'} type="password" name={'newPasswordAgain'} onChange={this.handleChange} autoComplete="off" />
                </div>

                <div className={"form-item"}>
                    <div className={'upload-btn'} onClick={this.deleteHandler}>
                        <SVGIcon name={'DELETE_ICON'} className={'upload-icon'} />
                        Delete Account
                    </div>
                </div>

                <div className={'btn-wrapper'}>
                    <button className={'submit-btn'} type="submit">
                        <SVGIcon name={'SETTINGS_ICON'} className={'upload-icon'} />
                        Save Changes
                    </button>
                </div>

            </form>
        );

    }

}

export default SettingsForm;