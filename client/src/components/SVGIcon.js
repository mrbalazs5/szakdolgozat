import * as React from "react";

const getSvgs = () => ({
    IMAGE_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="30px"
                height="30px"
                viewBox="0 0 20 20"
            >
                <path
                    fill="#000"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    d="M138.242 3684.469a1 1 0 112 0 1 1 0 01-2 0zM142 3690l-3.314-3.136-5.613 5.667-3.466-3.518-3.607 3.512V3681h16v9zm0 7h-16v-1.648l3.578-3.434 3.457 3.434.026-.015.021.015 5.514-5.606 3.404 3.496V3697zm-18 2h20v-20h-20v20z"
                    transform="translate(-180 -3839) translate(56 160)"
                />
            </svg>
        )
    },
    DOCX_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 32 32"
            >
                <path
                    fill="#000"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    d="M19 26v2.003A1.995 1.995 0 0117.003 30H1.997A2 2 0 010 27.993V5.007C0 3.898.9 3 2.009 3H12v6.002c0 1.111.898 1.998 2.006 1.998H19v2H6.007A3.003 3.003 0 003 15.999V23A2.996 2.996 0 006.007 26H19zM13 3v5.997c0 .554.451 1.003.99 1.003H19l-6-7zM6.007 14C4.9 14 4 14.9 4 15.992v7.016A2 2 0 006.007 25h22.986C30.1 25 31 24.1 31 23.008v-7.016A2 2 0 0028.993 14H6.007zM6 16v7h2.995A1.998 1.998 0 0011 20.994v-2.988A2.003 2.003 0 008.995 16H6zm1 1v5h2.001A.997.997 0 0010 21v-3c0-.552-.443-1-.999-1H7zm7.005-1A1.998 1.998 0 0012 18.006v2.988c0 1.108.894 2.006 2.005 2.006h.99A1.998 1.998 0 0017 20.994v-2.988A2.003 2.003 0 0014.995 16h-.99zm-.006 1A.997.997 0 0013 18v3c0 .552.443 1 .999 1h1.002A.997.997 0 0016 21v-3c0-.552-.443-1-.999-1h-1.002zM23 21c-.003 1.117-.9 2-2.005 2h-.99A2.003 2.003 0 0118 20.994v-2.988c0-1.12.898-2.006 2.005-2.006h.99c1.11 0 2.002.895 2.005 2h-1c0-.552-.443-1-.999-1h-1.002A.997.997 0 0019 18v3c0 .552.443 1 .999 1h1.002A.997.997 0 0022 21h1zm3-1.5L24 16h1l1.5 2.625L28 16h1l-2 3.5 2 3.5h-1l-1.5-2.625L25 23h-1l2-3.5z"
                />
            </svg>
        )
    },
    PDF_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="32"
                height="32"
                viewBox="0 0 32 32"
            >
                <path
                    fill="#000"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    d="M21 26v2.003A1.995 1.995 0 0119.003 30H3.997A2 2 0 012 27.993V5.007C2 3.898 2.9 3 4.009 3H14v6.002c0 1.111.898 1.998 2.006 1.998H21v2h-8.993A3.003 3.003 0 009 15.999V23A2.996 2.996 0 0012.007 26H21zM15 3v5.997c0 .554.451 1.003.99 1.003H21l-6-7zm-3.005 11C10.893 14 10 14.9 10 15.992v7.016A2 2 0 0011.995 25h17.01C30.107 25 31 24.1 31 23.008v-7.016A2 2 0 0029.005 14h-17.01zM25 19v-2h4v-1h-5v7h1v-3h3v-1h-3zm-13-1v5h1v-3h1.995a2 2 0 100-4H12v2zm1-1v2h2.001A.997.997 0 0016 18c0-.552-.443-1-.999-1H13zm5-1v7h2.995A1.998 1.998 0 0023 20.994v-2.988A2.003 2.003 0 0020.995 16H18zm1 1v5h2.001A.997.997 0 0022 21v-3c0-.552-.443-1-.999-1H19z"
                />
            </svg>
        )
    },
    SEARCH_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 129 129"
                width="35px"
                height="35px"
            >
                <path d="M51.6 96.7c11 0 21-3.9 28.8-10.5l35 35c.8.8 1.8 1.2 2.9 1.2s2.1-.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-35-35c6.5-7.8 10.5-17.9 10.5-28.8 0-24.9-20.2-45.1-45.1-45.1-24.8 0-45.1 20.3-45.1 45.1 0 24.9 20.3 45.1 45.1 45.1zm0-82c20.4 0 36.9 16.6 36.9 36.9C88.5 72 72 88.5 51.6 88.5S14.7 71.9 14.7 51.6c0-20.3 16.6-36.9 36.9-36.9z" />
            </svg>
        )
    },
    USER_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 64 64"
                width="35px"
                height="35px"
            >
                <path d="M41.245 33.035a16 16 0 10-18.49 0A26.041 26.041 0 004 58a2 2 0 002 2h52a2 2 0 002-2 26.041 26.041 0 00-18.755-24.965zM20 20a12 12 0 1112 12 12.014 12.014 0 01-12-12zM8.09 56A22.03 22.03 0 0130 36h4a22.03 22.03 0 0121.91 20z" />
            </svg>
        )
    },
    ADD_USER_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 61.33 64"
                width="35px"
                height="35px"
            >
                <g data-name="Layer 2">
                    <g data-name="Layer 1">
                        <path
                            d="M42.67 56H48v5.33a2.67 2.67 0 105.33 0V56h5.34a2.67 2.67 0 000-5.33h-5.34v-5.34a2.67 2.67 0 00-5.33 0v5.34h-5.33a2.67 2.67 0 100 5.33zM29.33 26.67a13.34 13.34 0 1113.34-13.34 13.35 13.35 0 01-13.34 13.34zm0-21.34a8 8 0 108 8 8 8 0 00-8-8z"
                            className="cls-1"
                        />
                        <path
                            d="M32 58.67H5.33a24 24 0 0138.35-19.22 2.66 2.66 0 001.59.55 2.69 2.69 0 001.58-4.85A29.26 29.26 0 0027 29.42C11.58 30.62 0 44.13 0 59.6V61a2.74 2.74 0 002.67 3H32a2.67 2.67 0 002.67-2.67A2.67 2.67 0 0032 58.67z"
                            className="cls-1"
                        />
                    </g>
                </g>
            </svg>
        )
    },
    LOGIN_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="35"
                height="35"
                viewBox="0 0 24 24"
            >
                <path d="M19 5h-3a1 1 0 010-2h4a1 1 0 011 1v16a1 1 0 01-1 1h-4a1 1 0 010-2h3V5zm-5.414 6L9.293 6.707a1 1 0 111.414-1.414l6 6c.63.63.184 1.707-.707 1.707H4a1 1 0 010-2h9.586zm-1.226 3.732a1 1 0 111.28 1.536l-3 2.5a1 1 0 11-1.28-1.536l3-2.5z" />
            </svg>
        )
    },
    RESTAURANT_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="20"
                viewBox="0 0 18 20"
            >
                <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    transform="translate(-715 -3168) translate(100 3068) translate(612 98)"
                >
                    <path
                        fill="#1D1D1D"
                        d="M16 6v6c0 1.1.9 2 2 2h1v7c0 .55.45 1 1 1s1-.45 1-1V3.13c0-.65-.61-1.13-1.24-.98C17.6 2.68 16 4.51 16 6zm-5 3H9V3c0-.55-.45-1-1-1s-1 .45-1 1v6H5V3c0-.55-.45-1-1-1s-1 .45-1 1v6c0 2.21 1.79 4 4 4v8c0 .55.45 1 1 1s1-.45 1-1v-8c2.21 0 4-1.79 4-4V3c0-.55-.45-1-1-1s-1 .45-1 1v6z"
                    />
                </g>
            </svg>
        )
    },
    SETTINGS_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                viewBox="0 0 32 32"
                width="35"
                height="35"
                xmlSpace="preserve"
            >
                <path d="M27.526 18.036L27 17.732c-.626-.361-1-1.009-1-1.732s.374-1.371 1-1.732l.526-.304a2.999 2.999 0 001.098-4.098l-1-1.732a3.003 3.003 0 00-4.098-1.098L23 7.339a1.977 1.977 0 01-2 0 1.98 1.98 0 01-1-1.732V5c0-1.654-1.346-3-3-3h-2c-1.654 0-3 1.346-3 3v.608a1.98 1.98 0 01-1 1.732 1.98 1.98 0 01-2 0l-.526-.304a3.005 3.005 0 00-4.099 1.098l-1 1.732a2.998 2.998 0 001.098 4.098l.527.304c.626.361 1 1.009 1 1.732s-.374 1.371-1 1.732l-.526.304a2.998 2.998 0 00-1.098 4.098l1 1.732a3.004 3.004 0 004.098 1.098L9 24.661a1.977 1.977 0 012 0 1.98 1.98 0 011 1.732V27c0 1.654 1.346 3 3 3h2c1.654 0 3-1.346 3-3v-.608c0-.723.374-1.37 1-1.732a1.98 1.98 0 012 0l.526.304a3.005 3.005 0 004.098-1.098l1-1.732a2.998 2.998 0 00-1.098-4.098zM16 21c-2.757 0-5-2.243-5-5s2.243-5 5-5 5 2.243 5 5-2.243 5-5 5z" />
            </svg>
        )
    },
    CROSS_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                version="1.1"
                viewBox="0 0 11 11"
            >
                <path d="M2.2 1.19l3.3 3.3L8.8 1.2a.67.67 0 01.5-.2.75.75 0 01.7.7.66.66 0 01-.2.48L6.49 5.5 9.8 8.82c.13.126.202.3.2.48a.75.75 0 01-.7.7.67.67 0 01-.5-.2L5.5 6.51 2.21 9.8a.67.67 0 01-.5.2.75.75 0 01-.71-.71.66.66 0 01.2-.48L4.51 5.5 1.19 2.18A.66.66 0 011 1.7a.75.75 0 01.7-.7.67.67 0 01.5.19z"></path>
            </svg>
        )
    },
    LEFT_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="feather feather-chevron-left"
                viewBox="0 0 24 24"
            >
                <path d="M15 18L9 12 15 6"></path>
            </svg>
        )
    },
    UPLOAD_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                width="25"
                height="25"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="feather feather-upload"
                viewBox="0 0 24 24"
            >
                <path d="M21 15v4a2 2 0 01-2 2H5a2 2 0 01-2-2v-4" style={{fill: 'transparent'}}/>
                <path d="M17 8L12 3 7 8" />
                <path d="M12 3L12 15" />
            </svg>
        )
    },
    DESCRIPTION_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="20"
                viewBox="0 0 16 20"
            >
                <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    transform="translate(-104 -244) translate(100 100) translate(0 142)"
                >
                    <path
                        fill="#1D1D1D"
                        d="M8 16h8v2H8v-2zm0-4h8v2H8v-2zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"
                    ></path>
                </g>
            </svg>
        )
    },
    DELETE_ICON: {
        content: (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="25"
                height="25"
                viewBox="0 0 21 20"
            >
                <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
                    <g fill="#000" transform="translate(-179 -360)">
                        <g transform="translate(56 160)">
                            <path d="M130.35 216h2.1v-8h-2.1v8zm4.2 0h2.1v-8h-2.1v8zm-6.3 2h10.5v-12h-10.5v12zm2.1-14h6.3v-2h-6.3v2zm8.4 0v-4h-10.5v4H123v2h3.15v14h14.7v-14H144v-2h-5.25z"></path>
                        </g>
                    </g>
                </g>
            </svg>
        )
    }

});

class SVGIcon extends React.Component{

    render(){

        const { name, className, title, style, onClick } = this.props;

        const svgs = getSvgs();
        if (!svgs[name]) return null;

        const { content } = svgs[name];

        return(
            <div className={className} title={title} style={style} onClick={onClick}>
                {content}
            </div>
        );
    }

};

export default SVGIcon;
