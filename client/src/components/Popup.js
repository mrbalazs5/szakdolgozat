import React from 'react';
import './Popup.scss';
import SVGIcon from "./SVGIcon";
import classNames from 'classnames';

class Popup extends React.Component {

    render(){

        return (

            <div className={'popup-wrapper'}>
                <div className={classNames('popup', this.props.className)}>
                    <SVGIcon name={'CROSS_ICON'} className={'cross-icon'} onClick={this.props.close}/>
                    <div className={'data-wrapper'}>
                        { this.props.children }
                    </div>
                </div>
            </div>

        );

    }

}


export default Popup;