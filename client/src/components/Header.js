import React from 'react';
import Menu from './Menu';
import './Header.scss';
import AnimateLoad from './AnimateLoad';

class Header extends React.Component {

    render(){

        return(
            <header>
                <div className={"header-content"}>

                    <AnimateLoad>
                        <Menu />
                    </AnimateLoad>

                </div>
            </header>
        );

    }

}

export default Header;