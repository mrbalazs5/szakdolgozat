import React from 'react';
import './AddRestaurantForm.scss';
import SimpleReactValidator from 'simple-react-validator';
import SVGIcon from "./SVGIcon";
import classNames from 'classnames';
import {acceptedImageMimeTypes, acceptedDocumentMimeTypes} from "../utils/acceptedMimeTypes";

class AddRestaurantForm extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            //Form elements
            name: '',
            logo: '',
            website: '',
            country: 'Hungary',
            city: '',
            district: '',
            street: '',
            latitude: '',
            longitude: '',
            user: '',
            contactName: '',
            contactPhoneNumber: '',
            contactEmail: '',
            contacts: [],
            descriptionFile: '',
            descriptionFilePath: '',
            menuFile: '',
            menuFilePath: '',
            //Others
            cities: [],
            showSelect: false
        };

        this.validator = new SimpleReactValidator({
            validators: {
                imageIsValid: {
                    message: 'Avatar\'s image type must be png, jpeg or svg',
                    rule: (val, params, validator) => {
                        return acceptedImageMimeTypes.includes(val.type);
                    }
                },
                file: {
                    message: 'Document\'s type must be a generic text type(docx, pdf, txt,etc...)',
                    rule: (val, params, validator) => {
                        return acceptedDocumentMimeTypes.includes(val.type);
                    }
                }
            }
        });

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.fileButtonHandler = this.fileButtonHandler.bind(this);
        this.getCities = this.getCities.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.addContactHandler = this.addContactHandler.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
    }

    componentDidMount(){
        fetch('/api/checkToken')
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    const error = new Error(res.error);
                    throw error;
                }
            })
            .then(res => {

                if(res.user){
                    this.setState({
                        user: res.user.email
                    });
                }

            })
            .catch(err => {
            });
    }

    getCities(e){

        let expression = e.target.value;

        this.setState({city: expression});

        setTimeout(() => {
            fetch(`/api/get-cities?expression=${encodeURIComponent(expression.toLowerCase())}`, {
                method: 'GET',
                headers: {"Content-Type": "application/json"}
            })
                .then((response) => {
                    return response.json()
                })
                .then((response) => {

                    this.setState({
                        cities: response,
                        showSelect: true
                    });

                });
        }, 500);

    }

    handleFileChange(e){
        const input = e.target;
        const name = e.target.name;

        if (input.files && input.files[0]) {

            const file = input.files[0];

            this.setState({
                [name]: file
            }, () => {

                console.log('eeee');

                if(this.validator.fieldValid(name)){

                    console.log('aaaa');

                    let formData  = new FormData();

                    formData.append('document', file);

                    fetch('/api/uploadDocument',{
                        method: 'POST',
                        body: formData
                    })
                    .then((res) => {
                        return res.json();
                    })
                    .then((res) => {

                        if (res.filePath) {
                            this.setState({
                                [`${name}Path`]: res.filePath
                            })
                        }

                    });

                }else{
                    this.validator.showMessages();
                    this.forceUpdate();
                }

            });

        }

    }

    fileButtonHandler(inputId){
        document.getElementById(inputId).click();
    }

    handleChange(e){

        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {[name]: value}
        );

    }

    handleSelect(e){
        this.setState({city: e.target.innerText});
    }

    handleBlur(){

        setTimeout(() => {
            this.setState({showSelect: false});
        }, 300);

    }

    addContactHandler(){

        if(
            this.validator.fieldValid('contactName') &&
            this.validator.fieldValid('contactPhoneNumber') &&
            this.validator.fieldValid('contactEmail')
        ){
            this.setState({
                contacts: [...this.state.contacts, {
                    name: this.state.contactName,
                    phoneNumber: this.state.contactPhoneNumber,
                    email: this.state.contactEmail
                }]
            }, () => {
                document.getElementById('contactName').value = '';
                document.getElementById('contactPhoneNumber').value = '';
                document.getElementById('contactEmail').value = '';
            });
        }

    }

    handleImageChange(e){
        let input = e.target;
        let name = e.target.name;

        if (input.files && input.files[0]) {

            this.setState({
                [name]: input.files[0]
            });

        }
    }

    handleSubmit(e){
        e.preventDefault();

        if (this.validator.allValid()) {

            let formData  = new FormData();

            formData.append('image', this.state.logo);

            fetch('/api/uploadImage',{
                method: 'POST',
                body: formData
            })
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                if(res.filePath){

                    fetch('/api/restaurant/add-restaurant',{
                        method: 'POST',
                        body: JSON.stringify({
                            name: this.state.name,
                            logo: res.filePath,
                            website: this.state.website,
                            country: this.state.country,
                            city: this.state.city,
                            district: this.state.district,
                            street: this.state.street,
                            latitude: this.state.latitude,
                            longitude: this.state.longitude,
                            user: this.state.user,
                            contacts: this.state.contacts,
                            descriptionFile: this.state.descriptionFilePath,
                            menuFile: this.state.menuFilePath,
                        }),
                        headers: {"Content-Type": "application/json"}
                    })
                        .then((response) => {
                            return response.json()
                        })
                        .then((response) => {

                            if(response.errors){
                                return this.props.history.push({
                                    pathname: "/add-restaurant",
                                    state: {flashMessage: response}
                                });
                            }else if(response.success){

                                this.props.history.push({
                                    pathname: "/add-restaurant",
                                    state: {flashMessage: response}
                                });

                                window.location.reload();
                            }else{
                                console.log(response);
                            }

                        });

                }else if(res.errors){
                    return this.props.history.push({
                        pathname: "/profile/sign-up",
                        state: {flashMessage: res}
                    });
                }
            });

        }else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render(){
        return(
            <form id={'add-restaurant'} className={'add-restaurant-form form'} onSubmit={this.handleSubmit}>

                <h1 className={'form-title'}>Add new restaurant</h1>

                <div className={'alert'}>
                    {this.validator.message('name', this.state.name, 'required|string')}
                    {this.validator.message('logo', this.state.logo, 'required|imageIsValid')}
                    {this.validator.message('website', this.state.website, 'required|url')}
                    {this.validator.message('country', this.state.country, 'required|string')}
                    {this.validator.message('city', this.state.city, 'required|string')}
                    {this.validator.message('district', this.state.district, 'string')}
                    {this.validator.message('street', this.state.street, 'required|string')}
                    {this.validator.message('latitude', this.state.latitude, 'numeric')}
                    {this.validator.message('longitude', this.state.longitude, 'numeric')}
                    {this.validator.message('contactName', this.state.contactName, 'required|string')}
                    {this.validator.message('contactPhoneNumber', this.state.contactPhoneNumber, 'required|string')}
                    {this.validator.message('contactEmail', this.state.contactEmail, 'required|email')}
                    {this.validator.message('descriptionFile', this.state.descriptionFile, 'file')}
                    {this.validator.message('menuFile', this.state.menuFile, 'file')}
                </div>

                <div className={"restaurant-row form-item-wrapper"}>

                    <h1 className={'row-title'}>General</h1>

                    <div className={ classNames("form-item", !this.validator.fieldValid('name') ? 'invalid' : '') }>
                        <label htmlFor={'name'}>Restaurant's name</label>
                        <input id={'name'} type="text" name={'name'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('website') ? 'invalid' : '') }>
                        <label htmlFor={'website'}>Website link</label>
                        <input id={'website'} type="text" name={'website'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('logo') ? 'invalid' : '') }>
                        <label htmlFor={'logo'}>Logo</label>
                        <div className={'upload-btn'} onClick={() => this.fileButtonHandler('logo')}>
                            <SVGIcon name={'UPLOAD_ICON'} className={'upload-icon'} />
                            Upload Logo
                        </div>
                        <input id={'logo'} type="file" name={'logo'} onChange={this.handleImageChange} hidden />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('descriptionFile') ? 'invalid' : '') }>
                        <label htmlFor={'descriptionFile'}>Description file</label>
                        <div className={'upload-btn'} onClick={() => this.fileButtonHandler('descriptionFile')}>
                            <SVGIcon name={'DESCRIPTION_ICON'} className={'upload-icon'} />
                            Upload Description
                        </div>
                        <input type="file"
                               id="descriptionFile"
                               onChange={ this.handleFileChange }
                               multiple={false}
                               name={'descriptionFile'}
                               hidden={true}
                        />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('menuFile') ? 'invalid' : '') }>
                        <label htmlFor={'menuFile'}>Menu file</label>
                        <div className={'upload-btn'} onClick={() => this.fileButtonHandler('menuFile')}>
                            <SVGIcon name={'RESTAURANT_ICON'} className={'upload-icon'} />
                            Upload Menu
                        </div>
                        <input type="file"
                               id="menuFile"
                               onChange={ this.handleFileChange }
                               multiple={false}
                               name={'menuFile'}
                               hidden={true}
                        />
                    </div>

                </div>

                <div className={"restaurant-row form-item-wrapper"}>
                    <h1 className={'row-title'}>Location</h1>

                    <div className={ classNames("form-item", !this.validator.fieldValid('country') ? 'invalid' : '') }>
                        <label htmlFor={'country'}>Country</label>
                        <select id={'country'} value={this.state.country} name={'country'} onChange={this.handleChange} >
                            <option value="Hungary">Hungary</option>
                        </select>
                    </div>

                    <div className={ classNames("form-item", "city-wrapper", !this.validator.fieldValid('city') ? 'invalid' : '') }>
                        <label htmlFor={'city'}>City</label>
                        <input onBlur={this.handleBlur} type="text" name="city" value={this.state.city} onChange={this.getCities} />
                        <ul id="cities" className={'select'} style={this.state.showSelect && (this.state.cities.length > 0) ? {display: 'block'} : {display: 'none'}}>
                            {
                                this.state.cities.map((city, index) => (
                                    <li onClick={this.handleSelect} key={index}>{city.name}</li>
                                ))
                            }
                        </ul>
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('district') ? 'invalid' : '') }>
                        <label htmlFor={'district'}>District</label>
                        <input id={'district'} type="text" name={'district'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('street') ? 'invalid' : '') }>
                        <label htmlFor={'street'}>Street and number</label>
                        <input id={'street'} type="text" name={'street'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('latitude') ? 'invalid' : '') }>
                        <label htmlFor={'latitude'}>Latitude</label>
                        <input id={'latitude'} type="number" step="0.000001" name={'latitude'} value={this.state.latitude} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('longitude') ? 'invalid' : '') }>
                        <label htmlFor={'longitude'}>Longitude</label>
                        <input id={'longitude'} type="number" step="0.000001" name={'longitude'} value={this.state.longitude} onChange={this.handleChange} />
                    </div>

                </div>

                <div className={"restaurant-row form-item-wrapper"}>
                    <h1 className={'row-title'}>Contacts</h1>

                    <ul className={'contacts-list'}>
                        {
                            this.state.contacts.map((contact, index) => (
                                <li key={index}>
                                    <span><b>Name:</b>{contact.name}</span>
                                    <span><b>Phone:</b>{contact.phoneNumber}</span>
                                    <span><b>Email:</b>{contact.email}</span>
                                </li>
                            ))
                        }
                    </ul>

                    <div className={ classNames("form-item", !this.validator.fieldValid('contactName') ? 'invalid' : '') }>
                        <label htmlFor={'contactName'}>Name</label>
                        <input id={'contactName'} type="text" name={'contactName'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('contactPhoneNumber') ? 'invalid' : '') }>
                        <label htmlFor={'contactPhoneNumber'}>Phone number</label>
                        <input id={'contactPhoneNumber'} type="text" name={'contactPhoneNumber'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('contactEmail') ? 'invalid' : '') }>
                        <label htmlFor={'contactEmail'}>Email</label>
                        <input id={'contactEmail'} type="text" name={'contactEmail'} onChange={this.handleChange} />
                    </div>

                    <div className={'btn-wrapper'}>
                        <div onClick={this.addContactHandler} className={'submit-btn add-contact-btn'}>
                            <SVGIcon name={'ADD_USER_ICON'} className={'upload-icon'} />
                            Add Contact
                        </div>
                    </div>

                </div>

                <div className={'btn-wrapper'}>
                    <button className={'submit-btn'} type="submit">
                        <SVGIcon name={'RESTAURANT_ICON'} className={'upload-icon'} />
                        Add Restaurant
                    </button>
                </div>

            </form>
        );
    }

}

export default AddRestaurantForm;