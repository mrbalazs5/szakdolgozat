import React, { Component } from 'react';
import SimpleReactValidator from 'simple-react-validator';
import './LoginForm.scss';
import classNames from "classnames";
import SVGIcon from "./SVGIcon";

class LoginForm extends Component{

    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: ''
        }

        this.validator = new SimpleReactValidator();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(e){

        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {[name]: value}
        );

    }

    handleSubmit(e){
        e.preventDefault();

        if (this.validator.allValid()) {

            fetch('/api/profile/sign-in',{
                method: 'POST',
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password,
                }),
                headers: {"Content-Type": "application/json"}
            })
                .then((response) => {
                    return response.json()
                })
                .then((response) => {
                    if(response.errors){
                        return this.props.history.push({
                            pathname: "/profile/sign-in",
                            state: {flashMessage: response}
                        });
                    }else if(response.success){
                        this.props.history.push({
                            pathname: "/",
                            state: {flashMessage: response}
                        });

                        window.location.reload();

                    }
                });

        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render(){

        return(

            <form className={'login-form form'} onSubmit={this.handleSubmit}>

                <div className={ classNames("form-item", !this.validator.fieldValid('email') ? 'invalid' : '') }>
                    <label htmlFor={'email'}>Email</label>
                    <input id={'email'} type="email" name={'email'} onChange={this.handleChange} autoComplete="on" />
                </div>

                <div className={ classNames("form-item", !this.validator.fieldValid('password') ? 'invalid' : '') }>
                    <label htmlFor={'password'}>Password</label>
                    <input id={'password'} type="password" name={'password'} onChange={this.handleChange} autoComplete="off" />
                </div>

                <div className={'alert'}>
                    {this.validator.message('email', this.state.email, 'required|email')}
                    {this.validator.message('password', this.state.password, 'required')}
                </div>

                <div className={'btn-wrapper'}>
                    <button className={'submit-btn'} type="submit">
                        <SVGIcon name={'LOGIN_ICON'} className={'upload-icon'} />
                        Sign In
                    </button>
                </div>

            </form>

        );

    }

}

export default LoginForm;