import React from 'react';
import './MainBoard.scss';
import ChatBoard from "../ChatBoard";
import AnimateLoad from '../AnimateLoad';
import Page from '../Page';
import SearchPopup from '../SearchPopup';

class MainBoardPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            showSearch: false
        };

        this.showSearch = this.showSearch.bind(this);
        this.closePopup = this.closePopup.bind(this);

    }

    showSearch(){

        this.setState({
            showSearch: true
        });

    }

    closePopup(){
        this.setState({
            showSearch: false
        })
    }

    render(){

        return(
            <Page location={ this.props.location }>
                <AnimateLoad>
                    <ChatBoard showSearch={ this.showSearch } />
                </AnimateLoad>
                <SearchPopup show={ this.state.showSearch } close={this.closePopup}/>
            </Page>
        );

    }

}

export default MainBoardPage;