import React from 'react';
import './RestaurantsPage.scss';
import AnimateLoad from '../AnimateLoad';
import Page from '../Page';
import defaultLogo from '../../img/default-restaurant.png';
import SVGIcon from "../SVGIcon";
import {confirmAlert} from "react-confirm-alert";
import RestaurantSettingsPopup from "../RestaurantSettingsPopup";

class ProfilePage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            restaurants: [],
            activeRestaurant: null
        };

        this.handleRestaurantClick = this.handleRestaurantClick.bind(this);
        this.deleteHandler = this.deleteHandler.bind(this);
        this.wantsToDeleteHandler = this.wantsToDeleteHandler.bind(this);
        this.closePopup = this.closePopup.bind(this)
        this.changeRestaurant = this.changeRestaurant.bind(this)

    }

    componentDidMount(){
        fetch('/api/get-restaurants')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((restaurants) => {

                restaurants.forEach((restaurant) => {
                    restaurant['isHidden'] = true;
                });

                this.setState({
                    restaurants: restaurants
                });

            })
            .catch(err => {
                console.log(err);
            });
    }

    handleRestaurantClick(index){

        let newRestaurants = this.state.restaurants;
        newRestaurants[index]['isHidden'] = !newRestaurants[index]['isHidden'];

        this.setState({
            restaurants: newRestaurants
        });

    }

    deleteHandler(restaurant){
        confirmAlert({
            title: 'Confirm to delete',
            message: 'Are you sure you want to delete this restaurant?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.wantsToDeleteHandler(restaurant)
                },
                {
                    label: 'No',
                }
            ]
        });
    };

    wantsToDeleteHandler(restaurant){

        fetch(`/api/restaurant/delete-restaurant/${restaurant.id}`, {
            method: 'DELETE'
        })
            .then((response) => {
                return response.json()
            })
            .then((response) => {
                console.log(response);
                if(response.errors || response.success){
                    this.props.history.push({
                        pathname: "/profile/restaurants",
                        state: {flashMessage: response}
                    });

                    this.setState({
                        restaurants: this.state.restaurants.filter((rest) => {
                            return rest.id !== restaurant.id
                        })
                    })

                }
            })
            .catch((err) => {
                console.log(err);
            });

    }

    closePopup(){
        this.setState({
            activeRestaurant: null
        })
    }

    //changes a given restaurant in the restaurants array
    changeRestaurant(restaurantInput){

        let restaurants = this.state.restaurants;

        restaurants.forEach((restaurant, index) => {

            if(restaurant.id === restaurantInput.id){
                restaurants[index] = restaurantInput;
            }

        });

        this.setState({
            restaurants: restaurants
        })
    }

    render(){

        return(
            <Page location={ this.props.location }>
                <AnimateLoad>
                    <div className={'restaurants-page'}>

                        <div className={'restaurants'}>
                            {
                                this.state.restaurants.map((restaurant, index) => {

                                    const createdAt = new Date(restaurant.createdAt);
                                    const createdAtMonth = createdAt.toLocaleString('en-GB', { month: 'long'});
                                    const createdAtDay = createdAt.toLocaleString('en-GB', { day: '2-digit'});
                                    const createdAtYear = createdAt.toLocaleString('en-GB', { year: 'numeric'});

                                    return(
                                        <div key={index} className={'restaurant'}>

                                            <div className={'img-wrapper'}>
                                                <SVGIcon
                                                    name={'SETTINGS_ICON'}
                                                    className={'settings-icon'}
                                                    onClick={() => {this.setState({activeRestaurant: restaurant})}}
                                                />
                                                <SVGIcon
                                                    name={'CROSS_ICON'}
                                                    className={'cross-icon'}
                                                    onClick={() => this.deleteHandler(restaurant)}
                                                />
                                                <img src={ restaurant.logo ? restaurant.logo : defaultLogo } alt={ restaurant.name }/>
                                                <div
                                                    onClick={ () => { this.handleRestaurantClick(index) } }
                                                    className={'show-more'}
                                                >
                                                    Show Details
                                                </div>
                                            </div>

                                            <div className={'restaurant-data'}>
                                                <h2 className={'title'}>{ restaurant.name }</h2>

                                                <div className={'restaurant-full-data'}
                                                     style={
                                                         restaurant.isHidden
                                                             ? {opacity: 0, display: 'none'}
                                                             : {opacity: 1, display: 'block'}
                                                     }
                                                >
                                                    <div>
                                                        <div className={'data-name'}>Owner:</div>
                                                        <div>{ `${restaurant.user.firstName} ${restaurant.user.lastName}` }</div>
                                                    </div>
                                                    <div>
                                                        <div className={'data-name'}>Website:</div>
                                                        <a className={'site-link'} href={ restaurant.website }>{ restaurant.website }</a>
                                                    </div>
                                                    <div>
                                                        <div className={'data-name'}>Created at:</div>
                                                        <div>{ `${createdAtMonth} ${createdAtDay}, ${createdAtYear}` }</div>
                                                    </div>
                                                    <div>
                                                        <div className={'data-name'}>Address:</div>
                                                        <div>
                                                            {`${restaurant.address.country.name},
                                                              ${restaurant.address.city.name}
                                                              ${restaurant.address.district}
                                                              ${restaurant.address.street}
                                                               `}
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    );

                                })
                            }
                        </div>

                    </div>
                </AnimateLoad>
                <RestaurantSettingsPopup history={ this.props.history } close={this.closePopup} restaurant={this.state.activeRestaurant} changeRestaurant={this.changeRestaurant} />
            </Page>
        );

    }

}

export default ProfilePage;