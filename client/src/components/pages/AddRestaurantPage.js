import React from 'react';
import './AddRestaurantPage.scss';
import Page from '../Page';
import AddRestaurantForm from '../AddRestaurantForm';
import AnimateLoad from '../AnimateLoad';

class AddRestaurantPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"add-restaurant"}>
                    <AnimateLoad>
                        <AddRestaurantForm history={ this.props.history } />
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default AddRestaurantPage;