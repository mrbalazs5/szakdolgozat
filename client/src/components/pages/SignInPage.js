import React from 'react';
import './SignInPage.scss';
import LoginForm from '../LoginForm';
import AnimateLoad from '../AnimateLoad';
import Page from '../Page';

class SignUpPage extends React.Component {

    render(){

        return(
            <Page location={ this.props.location }>
                <div className={'sign-up-page'}>
                    <AnimateLoad>
                        <LoginForm history={ this.props.history } />
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default SignUpPage;