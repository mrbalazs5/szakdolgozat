import React from 'react';
import './FindRestaurantsPage.scss';
import Page from '../Page';
import AnimateLoad from '../AnimateLoad';
import RestaurantPopup from '../RestaurantPopup';
import defaultLogo from '../../img/default-restaurant.png';

class FindRestaurantsPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            restaurants: [],
            activeRestaurant: null
        };

        this.closePopup = this.closePopup.bind(this)

    }

    componentDidMount(){

        const {city, food} = this.props.match.params;

        fetch(`/api/find-restaurant/${city}/${food}`)
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((restaurants) => {

                this.setState({
                    restaurants: restaurants
                });

            })
            .catch((err) => {
                console.log(err);
            })
    }

    closePopup(){
        this.setState({
            activeRestaurant: null
        })
    }

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"find-restaurants-page"}>
                    <AnimateLoad>
                        <h1 className={'page-title'}>Restaurants</h1>
                        <div className={'restaurants'}>
                            {
                                this.state.restaurants.map((restaurant, index) => (
                                    <div
                                        key={index}
                                        className={'restaurant'}
                                        onClick={() => {this.setState({activeRestaurant: restaurant})}}
                                    >

                                        <div className={'img-wrapper'}>
                                            <img src={ restaurant.logo ? restaurant.logo : defaultLogo } alt={ restaurant.name }/>
                                            <div className={'overlay'}>{restaurant.name}</div>
                                        </div>

                                    </div>
                                ))
                            }
                        </div>
                    </AnimateLoad>
                    <RestaurantPopup close={this.closePopup} restaurant={this.state.activeRestaurant} />
                </div>
            </Page>
        );

    }

}

export default FindRestaurantsPage;