import React from 'react';
import './ProfilePage.scss';
import AnimateLoad from '../AnimateLoad';
import Page from '../Page';
import defaultAvatar from '../../img/default-avatar.png';

class ProfilePage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            gender: '',
            age: '',
            avatar: '',
            password: '',
            role: '',
            createdAt: ''
        }
    }

    componentDidMount(){
        fetch('/api/get-user-data')
            .then((res) => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then((user) => {

                const createdAt = new Date(user.createdAt);
                const createdAtMonth = createdAt.toLocaleString('en-GB', { month: 'long'});
                const createdAtDay = createdAt.toLocaleString('en-GB', { day: '2-digit'});
                const createdAtYear = createdAt.toLocaleString('en-GB', { year: 'numeric'});

                this.setState({
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    gender: user.gender,
                    age: user.age,
                    avatar: user.avatar,
                    password: user.password,
                    role: user.role,
                    createdAt: `${createdAtMonth} ${createdAtDay}, ${createdAtYear}`,
                    numberOfRestaurants: user.restaurants.length
                });

            })
            .catch(err => {
                console.log(err);
            });
    }

    render(){

        return(
            <Page location={ this.props.location }>
                <AnimateLoad>
                    <div className={'profile-page'}>

                        <h1 className={'form-title'}>My Profile</h1>

                        <img src={ this.state.avatar ? this.state.avatar : defaultAvatar } alt={'Profile'}/>
                        <h1>{`${this.state.firstName} ${this.state.lastName}`}</h1>
                        <div className={'data-wrapper'}>
                            <span className={'data-title'}>E-mail:</span>
                            <span>{this.state.email}</span>
                        </div>
                        <div className={'data-wrapper'}>
                            <span className={'data-title'}>Gender:</span>
                            <span>{this.state.gender}</span>
                        </div>
                        <div className={'data-wrapper'}>
                            <span className={'data-title'}>Age:</span>
                            <span>{this.state.age}</span>
                        </div>
                        <div className={'data-wrapper'}>
                            <span className={'data-title'}>Registration date:</span>
                            <span>{this.state.createdAt}</span>
                        </div>
                        <div className={'data-wrapper'}>
                            <span className={'data-title'}>Number of restaurants:</span>
                            <span>{this.state.numberOfRestaurants}</span>
                        </div>
                    </div>
                </AnimateLoad>
            </Page>
        );

    }

}

export default ProfilePage;