import React from 'react';
import './AddMenuPage.scss';
import Page from '../Page';
import AddMenuForm from '../AddMenuForm';
import AnimateLoad from '../AnimateLoad';

class AddMenuPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"add-menu"}>
                    <AnimateLoad>
                        <AddMenuForm history={ this.props.history } parentState={ this.props.location.state } />
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default AddMenuPage;