import React from 'react';
import './AdminPage.scss';
import Page from '../Page';

class AdminPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"admin"}>
                    Admin
                </div>
            </Page>
        );

    }

}

export default AdminPage;