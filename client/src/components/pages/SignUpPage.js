import React from 'react';
import './SignUpPage.scss';
import RegistrationForm from '../RegistrationForm';
import AnimateLoad from '../AnimateLoad';
import Page from '../Page';

class SignUpPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={'sign-up-page'}>
                    <AnimateLoad>
                        <RegistrationForm history={ this.props.history } />
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default SignUpPage;