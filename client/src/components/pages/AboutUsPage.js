import React from 'react';
import './AboutUsPage.scss';
import Page from '../Page';
import AnimateLoad from '../AnimateLoad';

class AboutUsPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"about-us"}>
                    <AnimateLoad>
                        <div className={'about-us-description'}>
                            <h2>ChefBot welcomes you!</h2>
                            <p>This is a school project which implements a chatbot based on Google's Dialogflow API</p>
                            <div className={'features-list'}>
                                <h3>You can:</h3>
                                <ul>
                                    <li>Register a new account through the chatbot or by manually via form</li>
                                    <li>Add your Restaurant</li>
                                    <li>Add new foods, recipes...</li>
                                    <li>Talk with the bot about foods, restaurants</li>
                                    <li>Search for foods, recipes or restaurants</li>
                                </ul>
                            </div>
                        </div>
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default AboutUsPage;