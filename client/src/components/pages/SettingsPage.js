import React from 'react';
import './SettingsPage.scss';
import Page from '../Page';
import SettingsForm from '../SettingsForm';
import AnimateLoad from '../AnimateLoad';

class SettingsPage extends React.Component {

    render(){

        return(
            <Page location={this.props.location} >
                <div className={"settings-page"}>
                    <AnimateLoad>
                        <SettingsForm history={ this.props.history } />
                    </AnimateLoad>
                </div>
            </Page>
        );

    }

}

export default SettingsPage;