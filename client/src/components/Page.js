import React from 'react';
import AnimateLoad from './AnimateLoad';
import Flash from './Flash';
import DidYouKnowPopup from './DidYouKnowPopup';

class Page extends React.Component {

    render(){

        let flashMessage = this.props.location.state ? this.props.location.state.flashMessage : null;

        return(
            <div className={'page'}>

                <AnimateLoad>
                    {
                        flashMessage && flashMessage.success ?
                            (<Flash messages={ flashMessage.success } className={'success'} />) : ''
                    }
                    {
                        flashMessage && flashMessage.errors ?
                            (<Flash messages={flashMessage.errors} className={'alert'}/>) : ''
                    }
                    {
                        flashMessage && flashMessage.warning ?
                            (<Flash messages={flashMessage.warning} className={'warning'}/>) : ''
                    }
                    {
                        flashMessage && flashMessage.info ?
                            (<Flash messages={ flashMessage.info } className={'info'} />) : ''
                    }
                    { this.props.children }
                </AnimateLoad>

                <DidYouKnowPopup/>

            </div>
        );

    }

}

export default Page;