import React from 'react';
import './DidYouKnowPopup.scss';
import SVGIcon from "./SVGIcon";
import classNames from "classnames";

const anonymusMessages = [
    'You can add files to your restaurant through ChefBot.',
    'You can ask ChefBot about restaurants, foods, drinks.',
    'You can register an account through ChefBot.',
    'You can ask Chefbot about restaurants in your city.',
    'You find foods, drinks or other products through ChefBot.',
];

const loggedInMessages = [
    'You can add restaurants on the Add Restaurant menu tab and you can change them on the My Restaurants page.',
    'You can add and modify products on the My Menus tab.',
    'You can list all your profile data on the My Profile tab.',
    'You can change your account setting on the Settings tab.'
];

class DidYouKnowPopup extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            messages: anonymusMessages,
            activeMessage: '',
            show: false,
        };

        this.getRandomInt = this.getRandomInt.bind(this);
        this.handleArrowClick = this.handleArrowClick.bind(this);
    }

    getRandomInt(min, max){
        min = Math.ceil(min);
        max = Math.floor(max);

        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    handleArrowClick(e){

        const messages = this.state.messages;

        this.setState({
            show: !this.state.show,
            activeMessage: this.state.show ? messages[this.getRandomInt(0, messages.length - 1)] : this.state.activeMessage
        }, () => {

            if(window.innerWidth < 576){


                if(!this.state.show){

                    this.popup.style.bottom = `${-1 * (this.popup.offsetHeight - 20)}px`;

                }else{
                    this.popup.style.bottom = '0';
                }

            }

        });

    }

    componentDidMount(){

        const messages = this.state.messages;

        if(window.innerWidth < 576){
            window.addEventListener('load', () => {
                this.popup.style.bottom = `${-1 * (this.popup.clientHeight - 20)}px`;
            });
        }

        window.addEventListener('resize', () => {

            if(!this.popup){
                this.popup = document.getElementById('did-you-know');
            }

            if(window.innerWidth > 576){
                this.popup.style.bottom = 'auto';
            }else{
                this.popup.style.bottom = `${-1 * (this.popup.clientHeight - 20)}px`;
            }
        });

        fetch('/api/checkToken')
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw new Error(res.error);
                }
            })
            .then(res => {

                if(res.user){
                    this.setState({
                        messages: [...this.state.messages, ...loggedInMessages]
                    })
                }

            })
            .catch(err => {});

        this.setState({
            activeMessage: messages[this.getRandomInt(0, messages.length - 1)]
        }, () => {
            setTimeout(() => {
                this.setState({
                    show: true
                }, () => {

                    if(window.innerWidth < 576){
                        this.popup.style.bottom = '0';
                    }

                });
            }, this.getRandomInt(1000, 15000));
        });

    }

    render(){
        return(
            <aside id={'did-you-know'} ref={(popupElement) => {this.popup = popupElement}} className={classNames('did-you-know-wrapper', this.state.show ? 'show' : '')}>
                <SVGIcon onClick={this.handleArrowClick} name={'LEFT_ICON'} className={'left-icon'}/>
                <h2>Did You Know?</h2>
                <div className={'did-you-know-message'}>
                    { this.state.activeMessage }
                </div>
            </aside>
        )
    }

}

export default DidYouKnowPopup;