import React, { Component } from 'react';
import SimpleReactValidator from 'simple-react-validator';
import './RegistrationForm.scss';
import defaultAvatar from '../img/default-avatar.png';
import SVGIcon from "./SVGIcon";
import classNames from 'classnames';
import {acceptedImageMimeTypes} from "../utils/acceptedMimeTypes";

class RegistrationForm extends Component{

    constructor(props){
        super(props);

        this.state = {
            avatarImgSrc: '',
            firstName: '',
            lastName: '',
            gender: 'female',
            age: '',
            avatar: '',
            email: '',
            password: '',
            passwordAgain: '',
        };

        this.validator = new SimpleReactValidator({
            validators: {
                passwordMatch: {
                    message: 'Passwords doesn\'t match!',
                    rule: (val, params, validator) => {
                        return val === this.state.password;
                    }
                },
                imageIsValid: {
                    message: 'Avatar \'s image type must be png, jpeg or svg',
                    rule: (val, params, validator) => {
                        return acceptedImageMimeTypes.includes(val.type);
                    }
                }
            }
        });

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.uploadButtonHandler = this.uploadButtonHandler.bind(this);

    }

    uploadButtonHandler(){
        document.getElementById('avatar').click();
    }

    handleChange(e){

        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {[name]: value}
        );

    }

    handleImageChange(e){
        let input = e.target;

        if (input.files && input.files[0]) {

            if(acceptedImageMimeTypes.includes(input.files[0].type)){

                let reader = new FileReader();

                reader.onload = function(e) {

                    this.setState({
                        avatarImgSrc: e.target.result
                    });

                }.bind(this);

                reader.readAsDataURL(input.files[0]);

            }

            this.setState({
                avatar: input.files[0]
            });

        }

    }

    handleSubmit(e){
        e.preventDefault();

        if (this.validator.allValid()) {

            let formData  = new FormData();

            formData.append('image', this.state.avatar);

            fetch('/api/uploadImage',{
                method: 'POST',
                body: formData
            })
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                if(res.filePath){

                    fetch('/api/profile/sign-up',{
                        method: 'POST',
                        body: JSON.stringify({
                            firstName: this.state.firstName,
                            lastName: this.state.lastName,
                            gender: this.state.gender,
                            age: this.state.age,
                            avatar: res.filePath,
                            email: this.state.email,
                            password: this.state.password,
                        }),
                        headers: {"Content-Type": "application/json"}
                    })
                    .then((response) => {
                        return response.json()
                    })
                    .then((response) => {

                        if(response.errors){
                            return this.props.history.push({
                                pathname: "/profile/sign-up",
                                state: {flashMessage: response}
                            });
                        }else if(response.success){
                            return this.props.history.push({
                                pathname: "/profile/sign-in",
                                state: {flashMessage: response}
                            });
                        }

                    });

                }else if(res.errors){
                    return this.props.history.push({
                        pathname: "/profile/sign-up",
                        state: {flashMessage: res}
                    });
                }
            });

        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render(){
        return(

            <form className={'registration-form form'} onSubmit={this.handleSubmit}>

                <div className={'preview'}>
                    <img className={'preview-avatar'}  src={this.state.avatarImgSrc ? this.state.avatarImgSrc : defaultAvatar} alt="Avatar Preview" />
                    <h1 className={'preview-name'}>{
                        this.state.firstName || this.state.lastName ?
                            this.state.firstName + ' ' + this.state.lastName :
                            'My Name'
                    }</h1>
                </div>

                <div className={'alert'}>
                    {this.validator.message('firstName', this.state.firstName, 'required|string')}
                    {this.validator.message('lastName', this.state.lastName, 'required|string')}
                    {this.validator.message('gender', this.state.gender, 'required|string|in:female,male,other')}
                    {this.validator.message('age', this.state.age, 'required|integer|between:3,150,num')}
                    {this.validator.message('avatar', this.state.avatar, 'required|imageIsValid')}
                    {this.validator.message('email', this.state.email, 'required|email')}
                    {this.validator.message('password', this.state.password, 'required|min:6')}
                    {this.validator.message('passwordAgain', this.state.passwordAgain, 'required|passwordMatch')}
                </div>

                <div className={"form-item-wrapper"}>
                    <div className={ classNames("form-item inline", !this.validator.fieldValid('firstName') ? 'invalid' : '') }>
                        <label htmlFor={'firstName'}>Firstname</label>
                        <input id={'firstName'} type="text" name={'firstName'} onChange={this.handleChange} />
                    </div>

                    <div className={ classNames("form-item inline", !this.validator.fieldValid('lastName') ? 'invalid' : '') }>
                        <label htmlFor={'lastName'}>Lastname</label>
                        <input id={'lastName'} type="text" name={'lastName'} onChange={this.handleChange} />
                    </div>
                </div>

                <div className="form-item-wrapper">
                    <div className={ classNames("form-item inline", !this.validator.fieldValid('gender') ? 'invalid' : '') }>
                        <label htmlFor={'gender'}>Gender</label>
                        <select id={'gender'} value={this.state.gender}  name={'gender'} onChange={this.handleChange} >
                            <option value="female">Female</option>
                            <option value="male">Male</option>
                            <option value="other">Other</option>
                        </select>
                    </div>

                    <div className={ classNames("form-item inline", !this.validator.fieldValid('age') ? 'invalid' : '') }>
                        <label htmlFor={'age'}>Age</label>
                        <input id={'age'} type="number" name={'age'} value={this.state.age} onChange={this.handleChange} />
                    </div>
                </div>


                <div className={ classNames("form-item", !this.validator.fieldValid('avatar') ? 'invalid' : '') }>
                    <label htmlFor={'avatar'}>Avatar</label>
                    <div className={'upload-btn'} onClick={this.uploadButtonHandler}>
                        <SVGIcon name={'UPLOAD_ICON'} className={'upload-icon'} />
                        Upload Avatar
                    </div>
                    <input id={'avatar'} type="file" name={'avatar'} onChange={this.handleImageChange} hidden />
                </div>

                <div className={ classNames("form-item", !this.validator.fieldValid('email') ? 'invalid' : '') }>
                    <label htmlFor={'email'}>Email</label>
                    <input id={'email'} type="email" name={'email'} onChange={this.handleChange} autoComplete="on" />
                </div>

                <div className={ classNames("form-item", !this.validator.fieldValid('password') ? 'invalid' : '') }>
                    <label htmlFor={'password'}>Password</label>
                    <input id={'password'} type="password" name={'password'} onChange={this.handleChange} autoComplete="off" />
                </div>

                <div className={ classNames("form-item", !this.validator.fieldValid('passwordAgain') ? 'invalid' : '') }>
                    <label htmlFor={'passwordAgain'}>Verify password</label>
                    <input id={'passwordAgain'} type="password" name={'passwordAgain'} onChange={this.handleChange} autoComplete="off" />
                </div>

                <div className={'btn-wrapper'}>
                    <button className={'submit-btn'} type="submit">
                        <SVGIcon name={'ADD_USER_ICON'} className={'upload-icon'} />
                        Sign Up
                    </button>
                </div>

            </form>

        );
    }

}

export default RegistrationForm;
