import React from 'react';
import './Flash.scss';
import classNames from 'classnames';

class Flash extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            show: true
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){

        this.setState({
            show: false
        });

    }

    componentDidMount () {
        window.scrollTo(0, 0);
    }

    render(){

        if(this.props.messages === null) return null;

        return(
            <div onClick={this.handleClick}
                 style={this.state.show ? {opacity: 1, maxHeight: 1000} : {opacity: 0,  maxHeight: 0, padding: 0, marginTop: 0}}
                 className={classNames('flash-messages', this.props.className)}>
                {
                    this.props.messages.map((message, index) => (
                        <div key={index} className={'flash-message'}>{ message }</div>
                    ))
                }
            </div>
        );

    }

}

export default Flash;