import React from 'react';
import './AnimateLoad.scss';


class AnimateLoad extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            didMount: false
        }
    }

    componentDidMount(){
        setTimeout(() => {
            this.setState({didMount: true})
        }, 50)
    }
    render(){
        const {didMount} = this.state;
        return (
            <div className={`fade-in ${didMount && 'visible'}`}>
                { this.props.children }
            </div>
        );
    }

}


export default AnimateLoad;