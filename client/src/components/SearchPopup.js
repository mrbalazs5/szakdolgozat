import React from 'react';
import './SearchPopup.scss';
import Popup from './Popup';
import SimpleReactValidator from 'simple-react-validator';
import classNames from "classnames";
import SVGIcon from "./SVGIcon";

class SearchPopup extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            searchExpression: '',
            searchResults: []
        };

        this.validator = new SimpleReactValidator();

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(e){

        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {[name]: value}
        );

    }

    handleSubmit(e){
        e.preventDefault();

        if (this.validator.allValid()) {

            fetch('/api/find-message',{
                method: 'POST',
                body: JSON.stringify({
                    searchText: this.state.searchExpression,
                }),
                headers: {"Content-Type": "application/json"}
            })
            .then((response) => {
                return response.json()
            })
            .then((results) => {

                this.setState({
                    searchResults: results
                });

            });

        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    render(){

        if(!this.props.show){
            return null;
        }

        return (

            <Popup className={'search-popup'} close={this.props.close}>
                <form className={'search-text-form form'} onSubmit={this.handleSubmit}>

                    <div className={'alert'}>
                        {this.validator.message('searchExpression', this.state.searchExpression, 'required')}
                    </div>

                    <div className={ classNames("form-item", !this.validator.fieldValid('searchExpression') ? 'invalid' : '') }>
                        <input
                            id={'search-expression'}
                            type="text"
                            name={'searchExpression'}
                            onChange={this.handleChange}
                            placeholder={'Search for messages in your history'}
                            autoComplete="on"
                        />
                    </div>

                    <div className={'btn-wrapper'}>
                        <button className={'submit-btn'} type="submit">
                            <SVGIcon name={'SEARCH_ICON'} className={'upload-icon'} />
                            Search
                        </button>
                    </div>

                    <div className={'search-results'}>
                        {
                            this.state.searchResults.map((message, index) => (
                                <div key={index} className={'message-result'}>
                                    {message.text}
                                </div>
                            ))
                        }
                    </div>

                </form>

            </Popup>

        );

    }

}


export default SearchPopup;