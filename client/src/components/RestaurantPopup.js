import React from 'react';
import './RestaurantPopup.scss';
import defaultLogo from '../img/default-restaurant.png';
import productTypes from "../utils/productTypes";
import Popup from './Popup';


class RestaurantPopup extends React.Component {

    render(){

        if(!this.props.restaurant){
            return null;
        }

        const restaurant = this.props.restaurant;

        const products = restaurant.menu ? restaurant.menu.products : null;

        let foods = products ? products.filter(product => product.type === productTypes['Food']) : [];
        let drinks = products ? products.filter(product => product.type === productTypes['Drink']) : [];
        let others = products ? products.filter(product => product.type === productTypes['Other']) : [];

        return (

            <Popup className={'restaurant-popup'} close={this.props.close}>

                <div className={'logo'}>
                    <img src={restaurant.logo ? restaurant.logo : defaultLogo} alt={restaurant.name}/>
                </div>
                <div className={'text-data'}>
                    <div className={'row'}>
                        <div className={'data-name'}>Name:</div>
                        <div>
                            {restaurant.name}
                        </div>
                    </div>

                    <div className={'row'}>
                        <div className={'data-name'}>Address:</div>
                        <div>
                            {
                                `${restaurant.address.country.name},
                                                          ${restaurant.address.city.name}
                                                          ${restaurant.address.district}
                                                          ${restaurant.address.street}
                               `}
                        </div>
                    </div>

                    {
                        restaurant.descriptionFile ? (
                            <div className={'row'}>
                                <div className={'data-name'}>Description file:</div>
                                <div>
                                    <a href={restaurant.descriptionFile} target="_blank" rel="noopener noreferrer">
                                        Description file
                                    </a>
                                </div>
                            </div>
                        ) : ''
                    }

                    {
                        restaurant.menuFile ? (
                            <div className={'row'}>
                                <div className={'data-name'}>Menu file:</div>
                                <div>
                                    <a href={restaurant.menuFile} target="_blank" rel="noopener noreferrer">
                                        Menu file
                                    </a>
                                </div>
                            </div>
                        ) : ''
                    }

                    <div className={'row'}>
                        <div className={'data-name'}>Website:</div>
                        <div>
                            <a href={restaurant.website} target="_blank" rel="noopener noreferrer">
                                {restaurant.website}
                            </a>
                        </div>
                    </div>
                    <div className={'menu'}>
                        <h2>Menu</h2>

                        {
                            foods.length > 0 ? (
                                <div className={'products'}>
                                    <h3>Foods</h3>
                                    {
                                        foods.map((food, index) => (
                                            <div key={index} className={'product'}>
                                                <span>{food.name}</span>
                                                <span>{food.price} €</span>
                                            </div>
                                        ))
                                    }
                                </div>
                            ) : ''

                        }

                        {
                            drinks.length > 0 ? (
                                <div className={'products'}>
                                    <h3>Drinks</h3>
                                    {
                                        drinks.map((drink, index) => (
                                            <div key={index} className={'product'}>
                                                <span>{drink.name}</span>
                                                <span>{drink.price}</span>
                                            </div>
                                        ))
                                    }
                                </div>
                            ) : ''

                        }

                        {
                            others.length > 0 ? (
                                <div className={'products'}>
                                    <h3>Others</h3>
                                    {
                                        others.map((other, index) => (
                                            <div key={index} className={'product'}>
                                                <span>{other.name}</span>
                                                <span>{other.price}</span>
                                            </div>
                                        ))
                                    }
                                </div>
                            ) : ''

                        }

                        {
                            foods.length === 0 && drinks.length === 0 && others.length === 0 ? (
                                <div className={'empty-text'}>This restaurant's menu is empty.</div>
                            ) : ''
                        }

                    </div>
                </div>

            </Popup>

        );

    }

}


export default RestaurantPopup;