import React from 'react';
import Header from './components/Header';
import MainBoardPage from './components/pages/MainBoardPage';
import SignUpPage from './components/pages/SignUpPage';
import SignInPage from './components/pages/SignInPage';
import AddRestaurantPage from './components/pages/AddRestaurantPage';
import AddMenuPage from './components/pages/AddMenuPage';
import AboutUsPage from './components/pages/AboutUsPage';
import RestaurantsPage from './components/pages/RestaurantsPage';
import SettingsPage from './components/pages/SettingsPage';
import AdminPage from './components/pages/AdminPage';
import ProfilePage from './components/pages/ProfilePage';
import FindRestaurantsPage from './components/pages/FindRestaurantsPage';
import userRoles from './utils/userRoles';
import { Switch, Route } from 'react-router-dom';
import withAuth from './utils/withAuth';
import "./global-styles/styles.scss";

class App extends React.Component {

  render(){

      return (
          <div>
            <Header />

            <Switch>

                <Route exact path='/' component={ MainBoardPage }/>
                <Route exact path='/profile' component={withAuth(ProfilePage, userRoles['user'])}/>
                <Route exact path='/profile/restaurants' component={withAuth(RestaurantsPage, userRoles['user'])}/>
                <Route exact path='/profile/settings' component={withAuth(SettingsPage, userRoles['user'])}/>
                <Route exact path='/profile/sign-up' component={SignUpPage}/>
                <Route exact path='/profile/sign-in' component={SignInPage}/>
                <Route exact path='/add-restaurant' component={withAuth(AddRestaurantPage, userRoles['user'])}/>
                <Route exact path='/add-menu' component={withAuth(AddMenuPage, userRoles['user'])}/>
                <Route exact path='/about-us' component={AboutUsPage}/>
                <Route exact path='/admin' component={withAuth(AdminPage, userRoles['admin'])}/>
                <Route exact path='/find-restaurant/:city/:food?' component={FindRestaurantsPage}/>

            </Switch>

          </div>
      );

  }

}

export default App;
